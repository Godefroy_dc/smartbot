import { createConnection, EntitySchema, ObjectType, Repository } from 'typeorm'
import settings from './settings'

// Errors handling
process.on('uncaughtException', err => console.error(`Uncaught Exception: ${err.stack}`))
process.on('unhandledRejection', (reason, p) =>
  console.error(`Unhandled Rejection: ${JSON.stringify(p)} (Reason: ${reason})`)
)

const db = createConnection(settings.database).catch(e => {
  throw new Error('Database connection error' + e.message)
})

export async function getRepository<Entity>(
  target: ObjectType<Entity> | EntitySchema<Entity> | string
): Promise<Repository<Entity>> {
  const connection = await db
  return connection.getRepository(target)
}

export async function getCustomRepository<T extends Repository<E>, E>(customRepository: ObjectType<T>): Promise<T> {
  const connection = await db
  return connection.getCustomRepository(customRepository)
}
