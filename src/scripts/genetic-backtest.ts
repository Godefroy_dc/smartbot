import chalk from 'chalk'
import * as path from 'path'
import GeneticBotAdvisor from '../botadvisors/genetic/GeneticBotAdvisor'
import Bot from '../bots/Bot'
import settings from '../settings'
import { summarize } from './utils/summarize'

settings.verbose = false

const filepath = path.resolve(settings.dataDir, 'genetic/bot1.json')
const startDate = new Date('2019-11-01')
const endDate = new Date('2019-12-15')

async function run() {
  console.log(chalk.cyan('GeneticBot backtesting...'))

  const botAdvisor = await GeneticBotAdvisor.loadFile(filepath)

  const bot = new Bot(botAdvisor)

  const exchange = bot.tradingExchange
  const initialBalances: [number, number] = [exchange.account1.balance, exchange.account2.balance]
  console.log(chalk.cyan(`Period: ${startDate.toLocaleDateString()} - ${endDate.toLocaleDateString()}`))
  console.log(
    chalk.cyan(
      `Initial balance: ${exchange.account1.balance} ${exchange.account1.symbol} + ${exchange.account2.balance} ${exchange.account2.symbol}`
    )
  )

  // Start Backtest
  bot.start()
  await botAdvisor.backtest(startDate, endDate, botAdvisor.lookBack)
  bot.stop()

  // Summary
  exchange.clearFirstCandlesticks(botAdvisor.lookBack)
  summarize(bot, initialBalances)
}

run().catch(error => console.error(error))
