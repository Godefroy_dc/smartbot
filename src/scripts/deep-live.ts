import chalk from 'chalk'
import DeepBotAdvisor from '../botadvisors/deep/DeepBotAdvisor'
import Bot from '../bots/Bot'
import settings from '../settings'

settings.verbose = true

async function run() {
  console.log('Loading DeepBot...')
  const botAdvisor = await DeepBotAdvisor.fromDir('bot1')

  const bot = new Bot(botAdvisor, true)
  bot.start()
  await bot.watch()
  console.log(chalk.green('DeepBot is running'))
}

run().catch(error => console.error(error))
