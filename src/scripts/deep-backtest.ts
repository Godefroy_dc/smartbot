import chalk from 'chalk'
import DeepBotAdvisor from '../botadvisors/deep/DeepBotAdvisor'
import Bot from '../bots/Bot'
import settings from '../settings'
import { summarize } from './utils/summarize'

settings.verbose = false

// const startDate = new Date('2013-09-01')
// const endDate = new Date('2019-01-01')
const startDate = new Date('2019-01-01')
const endDate = new Date('2019-11-01')

async function run() {
  console.log(chalk.cyan('DeepBot backtesting...'))

  const botAdvisor = await DeepBotAdvisor.fromDir('bot1')

  const bot = new Bot(botAdvisor)

  const exchange = bot.tradingExchange
  const initialBalances: [number, number] = [exchange.account1.balance, exchange.account2.balance]
  console.log(chalk.cyan(`Period: ${startDate.toLocaleDateString()} - ${endDate.toLocaleDateString()}`))
  console.log(
    chalk.cyan(
      `Initial balance: ${exchange.account1.balance} ${exchange.account1.symbol} + ${exchange.account2.balance} ${exchange.account2.symbol}`
    )
  )

  // Start Backtest
  bot.start()
  await botAdvisor.backtest(startDate, endDate, botAdvisor.config.lookBack)
  bot.stop()

  // Summary
  summarize(bot, initialBalances)
}

run().catch(error => console.error(error))
