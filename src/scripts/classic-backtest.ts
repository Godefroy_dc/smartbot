import chalk from 'chalk'
import ClassicBotAdvisor from '../botadvisors/ClassicBotAdvisor'
import Bot from '../bots/Bot'
import { CandlestickSizes } from '../interfaces'
import settings from '../settings'
import { summarize } from './utils/summarize'

settings.verbose = false

const startDate = new Date('2019-01-01')
const endDate = new Date('2019-12-01')

async function run() {
  console.log(chalk.cyan('ClassicBot backtesting...'))

  const botAdvisor = new ClassicBotAdvisor({
    tradingExchange: {
      exchange: 'bitfinex',
      currency1: 'USD',
      currency2: 'XRP'
    },
    watchedExchanges: [],
    candlestickSize: CandlestickSizes.d1,
    balanceToTrade: 0.5,
    // goalMove: 1.02,
    stoplossPeriod: 100
  })

  const bot = new Bot(botAdvisor)

  const exchange = bot.tradingExchange
  const initialBalances: [number, number] = [exchange.account1.balance, exchange.account2.balance]
  console.log(chalk.cyan(`Period: ${startDate.toLocaleDateString()} - ${endDate.toLocaleDateString()}`))
  console.log(
    chalk.cyan(
      `Initial balance: ${exchange.account1.balance} ${exchange.account1.symbol} + ${exchange.account2.balance} ${exchange.account2.symbol}`
    )
  )

  // Start Backtest
  bot.start()
  await botAdvisor.backtest(startDate, endDate)
  bot.stop()

  // Summary
  summarize(bot, initialBalances)
}

run().catch(error => console.error(error))
