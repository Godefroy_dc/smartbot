import chalk from 'chalk'
import GeneticBotAdvisor from '../botadvisors/genetic/GeneticBotAdvisor'
import { IGeneticBotConfig } from '../botadvisors/genetic/interfaces'
import { CandlestickSizes } from '../interfaces'
import settings from '../settings'

settings.verbose = false

const config: IGeneticBotConfig = {
  startDate: new Date('2018-01-01'),
  endDate: new Date('2019-12-01'),
  tradingExchange: {
    exchange: 'bitfinex',
    currency1: 'USD',
    currency2: 'BTC'
  },
  watchedExchanges: [],
  candlestickSize: CandlestickSizes.d1,
  validationSplit: 0.15,
  // goalMove: 1.01,
  // stoplossPeriod: 12,
  breakBacktestMaxDrawdown: 0.9,
  genetic: {
    crossover: 0.6,
    iterations: 100000,
    mutation: 0.3,
    size: 50,
    fittestAlwaysSurvives: true
  },
  genes: {
    types: ['ema', 'rsi', 'rsiVolume', 'mfi', 'emaCross', 'smaCross', 'momentum'],
    minBuy: 1,
    maxBuy: 4,
    minSell: 1,
    maxSell: 4
  }
}

async function run() {
  console.log(chalk.cyan('GeneticBot training...'))
  const botAdvisor = new GeneticBotAdvisor(config)

  await botAdvisor.train()
  console.log(chalk.green('GeneticBot trained and saved.'))
}

run().catch(error => console.error(error))
