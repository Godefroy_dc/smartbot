import chalk from 'chalk'
import DeepBotAdvisor, { IDeepBotConfig } from '../botadvisors/deep/DeepBotAdvisor'
import { CandlestickSizes } from '../interfaces'
import settings from '../settings'
import { loadPredicoin } from './utils/load-predicoin'

settings.verbose = true

async function run() {
  const startDate = new Date('2018-01-01')
  const endDate = new Date('2019-06-01')

  const predicoinData = await loadPredicoin('BTC', startDate, endDate)

  const config: IDeepBotConfig = {
    tradingExchange: {
      exchange: 'bitfinex',
      currency1: 'USD',
      currency2: 'BTC'
    },
    watchedExchanges: [],
    watchedData: predicoinData,
    candlestickSize: CandlestickSizes.h1,
    balanceToTrade: 0.99999,
    lookBack: 0,
    lookForward: 6,
    goalMove: 1.03,
    stoplossPeriod: 12,
    training: {
      epochs: 200,
      batchSize: 128,
      validationSplit: 0.5
    }
  }

  console.log(chalk.cyan('DeepBot training...'))
  const botAdvisor = new DeepBotAdvisor(config)
  await botAdvisor.train(startDate, endDate)

  console.log(chalk.green('DeepBot trained.'))
}

run().catch(error => console.error(error))
