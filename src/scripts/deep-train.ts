import chalk from 'chalk'
import DeepBotAdvisor, { IDeepBotConfig } from '../botadvisors/deep/DeepBotAdvisor'
import { CandlestickSizes } from '../interfaces'
import settings from '../settings'

settings.verbose = true

const startDate = new Date('2013-09-01')
const endDate = new Date('2019-01-01')
// const startDate = new Date('2019-01-01')
// const endDate = new Date('2019-10-01')

const config: IDeepBotConfig = {
  tradingExchange: {
    exchange: 'bitfinex',
    currency1: 'USD',
    currency2: 'LTC'
  },
  watchedExchanges: [
    {
      exchange: 'bitfinex',
      currency1: 'USD',
      currency2: 'LTC'
    },
    {
      exchange: 'bitfinex',
      currency1: 'USD',
      currency2: 'BTC'
    }
  ],
  candlestickSize: CandlestickSizes.h1,
  balanceToTrade: 0.99999,
  lookBack: 50,
  lookForward: 3,
  goalMove: 1.05,
  stoplossPeriod: 100,
  training: {
    epochs: 200,
    batchSize: 128,
    validationSplit: 0.1
  }
}

async function run() {
  console.log(chalk.cyan('DeepBot training...'))
  const botAdvisor = new DeepBotAdvisor(config)
  await botAdvisor.train(startDate, endDate)

  console.log(chalk.green('DeepBot trained.'))
}

run().catch(error => console.error(error))
