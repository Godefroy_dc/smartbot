import chalk from 'chalk'
import { writeFileSync } from 'fs'
import Bot from '../../bots/Bot'
import Exchange from '../../exchanges/Exchange'

const chartDataFile = __dirname + '/../chart/data.js'

function saveChartData(exchange: Exchange, initialBalances: [number, number], points = 500) {
  // Candlesticks data
  const seriesCandles = exchange.candlesticks.map(c => ({
    x: c.date.getTime(),
    y: [c.open, c.high, c.low, c.close]
  }))
  // Trades data
  const tradesPoints = exchange.trades.map(t => ({
    x: t.date.getTime(),
    y: t.rate,
    side: t.side,
    amount: t.amount
  }))

  // Balance evolution data
  const seriesBalance: Array<{ x: number; y: number }> = []
  const seriesBalance2: Array<{ x: number; y: number }> = []
  const currentBalances = [...initialBalances]
  let tradeI = 0

  for (const candlestick of exchange.candlesticks) {
    while (tradeI < exchange.trades.length && exchange.trades[tradeI].date < candlestick.date) {
      const diff = exchange.getTradeBalancesDiff(exchange.trades[tradeI])
      currentBalances[0] += diff[0]
      currentBalances[1] += diff[1]
      tradeI++
    }

    seriesBalance.push({
      x: candlestick.date.getTime(),
      y: Math.round(currentBalances[0] + currentBalances[1] * candlestick.open)
    })
    seriesBalance2.push({
      x: candlestick.date.getTime(),
      y: Math.round(currentBalances[1] * candlestick.open)
    })
  }

  writeFileSync(
    chartDataFile,
    `
    const title = ${JSON.stringify(`${exchange.account1.symbol}/${exchange.account2.symbol}`)}
    const currency1 = '${exchange.account1.symbol}'
    const currency2 = '${exchange.account2.symbol}'
    const seriesCandles = ${JSON.stringify(seriesCandles)}
    const seriesBalance = ${JSON.stringify(seriesBalance)}
    const seriesBalance2 = ${JSON.stringify(seriesBalance2)}
    const tradesPoints = ${JSON.stringify(tradesPoints)}
  `
  )
}

export function summarize(bot: Bot, initialBalances: [number, number]) {
  const exchange = bot.tradingExchange
  const firstCandlestick = exchange.candlesticks[0]
  const lastCandlestick = exchange.candlesticks[exchange.candlesticks.length - 1]
  const initialBalancesValue = initialBalances[0] + initialBalances[1] * firstCandlestick.close
  const endingBalancesValue = exchange.account1.balance + exchange.account2.balance * lastCandlestick.close
  const profit = (endingBalancesValue - initialBalancesValue) / initialBalancesValue
  const holdProfit = (lastCandlestick.close - firstCandlestick.close) / firstCandlestick.close

  // Generate chart data
  saveChartData(exchange, initialBalances)

  console.log(
    chalk.yellow(
      `Ending balance: ${exchange.account1.balance} ${exchange.account1.symbol} + ${exchange.account2.balance} ${exchange.account2.symbol}`
    )
  )
  console.log(chalk.yellow(`Trades: ${exchange.trades.length}`))
  console.log(chalk[profit > 0 ? 'green' : 'red'](`Profit: ${(profit * 100).toFixed(1)}%`))
  console.log(`Hold profit: ${(holdProfit * 100).toFixed(1)}%`)
  console.log(`Exposure: ${Math.round(bot.getExposure() * 100)}%`)
  console.log(`Max drawdown: ${Math.round(bot.maxDrawdown * 100)}%`)

  console.log(chalk.gray('---'))
  console.log('Comments:')

  if (exchange.trades.length === 0) {
    console.log(chalk.red('- No trade! Oh no...'))
  } else {
    if (profit > 0.5) console.log(chalk.green(`- That's a pretty good profit!`))
    else if (profit > 0.2) console.log(chalk.green(`- That's a good profit!`))
    else if (profit > 0.1) console.log(chalk.yellow(`- That's a profit!`))
    else if (profit > 0) console.log(chalk.yellow(`- You lose nothing.`))

    if (profit > 0 && holdProfit < 0) console.log(chalk.green('- Very good for a down trend.'))
    else if (holdProfit > profit) console.log(chalk.red('- You should have bought and hodl.'))
    else console.log(chalk.yellow('- Better than hodl.'))

    if (exchange.trades.length < 30) console.log(chalk.yellow('- Maybe not enough trades.'))
  }
}
