import { getRepository } from '../../database'
import { Predicoin } from '../../model/entity/Predicoin'

export async function loadPredicoin(currency: string, startDate: Date, endDate: Date) {
  const repository = await getRepository(Predicoin)

  const data = await repository
    .createQueryBuilder()
    .where({
      ticker: currency
    })
    .andWhere('timestamp_utc >= :startDate AND timestamp_utc <= :endDate', { startDate, endDate })
    .orderBy({ timestamp_utc: 'ASC' })
    .getMany()

  return data.map(d => ({
    // Convert date to UTC
    date: new Date(d.timestamp_utc.getTime() - d.timestamp_utc.getTimezoneOffset() * 60000),
    inputs: [
      d.overall_score || 0,
      d.technical_raw || 0,
      d.news_sentiment_raw || 0,
      d.twitter_sentiment_raw || 0,
      d.reddit_sentiment_raw || 0,
      d.buzz_raw || 0
    ]
  }))
}
