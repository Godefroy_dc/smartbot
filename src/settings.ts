import * as dotenv from 'dotenv'
import * as path from 'path'
import { ConnectionOptions } from 'typeorm'

dotenv.config()

export interface IExchangeSettings {
  name: string
  apiKey: string
  apiSecret: string
}

export default {
  verbose: false,

  server: {
    port: 1337
  },

  dataDir: path.resolve(__dirname, '../data'),

  database: {
    type: 'postgres',
    host: process.env.DB_HOST || 'localhost',
    port: process.env.DB_PORT || 5432,
    username: process.env.DB_USERNAME || 'smartbot',
    password: process.env.DB_PASSWORD || 'smartbot',
    database: process.env.DB_DATABASE || 'smartbot',
    // logging: ['query', 'error'],
    synchronize: true,
    entities: [__dirname + '/model/entity/*.ts']
  } as ConnectionOptions,

  slack: {
    slackbotUrl: 'https://lonestone.slack.com/services/hooks/slackbot',
    slackbotToken: process.env.SLACKBOT_TOKEN,
    recipients: ['@godefroy']
  },

  // Candlesticks sizes in ms
  candlestickSizes: {
    min1: 60000,
    min5: 5 * 60000,
    min15: 15 * 60000,
    min30: 30 * 60000,
    h1: 60 * 60000,
    h6: 6 * 60 * 60000,
    h12: 12 * 60 * 60000,
    d1: 24 * 60 * 60000
  },

  exchanges: [
    {
      name: 'poloniex',
      apiKey: process.env.POLONIEX_KEY,
      apiSecret: process.env.POLONIEX_SECRET
    },
    {
      name: 'bitfinex',
      apiKey: process.env.BITFINEX_KEY,
      apiSecret: process.env.BITFINEX_SECRET
    },
    {
      name: 'kraken',
      apiKey: process.env.KRAKEN_KEY,
      apiSecret: process.env.KRAKEN_SECRET
    }
  ] as IExchangeSettings[]
}
