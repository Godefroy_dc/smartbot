import importlib.util
import json
import os
import sys
from math import sqrt

from keras.layers import LSTM, Dense, Dropout, LeakyReLU
from keras.models import Sequential
from matplotlib import pyplot
from pandas import DataFrame, concat, read_csv
from sklearn.metrics import mean_squared_error

# Load Keras without printing on stderr
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
stderr = sys.stderr
sys.stderr = sys.stdout
sys.stderr = stderr

dir_path = os.path.dirname(os.path.realpath(__file__))
model_path = dir_path + '/../../../data/deep/bot1'

# Config
CONFIG_FILE = model_path + '/config.json'
TRAIN_FILE = model_path + '/train.csv'
OUTPUT_FILE = model_path + '/output.csv'

# Load config from JSON
with open(model_path + '/config.json') as json_file:
    config = json.load(json_file)
    OUTPUTS = len(config['state']['scaleStats']['outputs'])
    EPOCHS = config['config']['training']['epochs']
    BATCH_SIZE = config['config']['training']['batchSize']
    VALIDATION_SPLIT = config['config']['training']['validationSplit']

# Load dataset
dataset = read_csv(TRAIN_FILE, header=0, index_col=0)
values = dataset.values.astype('float32')

# Extract X and Y
X = values[:, :-OUTPUTS]
Y = values[:, -OUTPUTS:]

model = Sequential([
    Dense(200, input_dim=X.shape[1], activation='relu'),

    # Dense(layers[1], activation='relu'),
    # LeakyReLU(alpha=0.001),
    # Dropout(0.2),

    Dense(Y.shape[1], activation='sigmoid')
])

model.compile(loss='binary_crossentropy',
              optimizer='adam', metrics=['accuracy'])

# Fit network
history = model.fit(X, Y,
                    validation_split=VALIDATION_SPLIT, epochs=EPOCHS, batch_size=BATCH_SIZE, shuffle=True, verbose=2)

# Plot history
pyplot.plot(history.history['loss'], label='train')
pyplot.plot(history.history['val_loss'], label='test')
pyplot.legend()
pyplot.show()
pyplot.plot(history.history['acc'], label='train')
pyplot.plot(history.history['val_acc'], label='test')
pyplot.legend()
pyplot.show()

# Save model
model.save(model_path + '/model.h5')
model.save_weights(model_path + '/model.hdf5')
with open(model_path + '/model.json', 'w') as f:
    f.write(model.to_json())

spec = importlib.util.spec_from_file_location(
    'encoder', dir_path + '/../../../node_modules/keras-js/encoder.py')
module = importlib.util.module_from_spec(spec)
spec.loader.exec_module(module)
encoder = module.Encoder(model_path + '/model.hdf5')
encoder.serialize()
encoder.save()

# Predict and save all sequence
predY = model.predict(X)
predDF = DataFrame(predY, columns=['output{}'.format(i)
                                   for i in range(0, OUTPUTS)])
predDF.to_csv(OUTPUT_FILE, index=False)
