import os

from matplotlib import pyplot
from pandas import read_csv

dir_path = os.path.dirname(os.path.realpath(
    __file__)) + '/../../../data/deep/bot1'

# load dataset
dataset = read_csv(dir_path + '/output.csv', header=0)
values = dataset.values

# specify columns to plot
groups = range(0, values.shape[1])
i = 1

# plot each column
pyplot.figure()
for group in groups:
    pyplot.subplot(len(groups), 1, i)
    pyplot.plot(values[:, group])
    pyplot.title(dataset.columns[group], y=0.5, loc='right')
    i += 1

pyplot.show()
