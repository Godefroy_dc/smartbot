import * as KerasJS from 'keras-js'
import { ICandlestick } from '../../interfaces'
import settings from '../../settings'
import { IScaleStats, mean, scale, scaleStd, std } from '../../stats'
import { alignDated, fileExists, mkdir, range, readFile, runPython, stringifyCsv, writeFile } from '../../utils'
import BotAdvisor, { IAdvice, IBotConfig } from '../BotAdvisor'

export interface IDeepBotConfig extends IBotConfig {
  watchedData?: Array<{ date: Date; inputs: number[] }>
  lookBack: number
  lookForward: number
  balanceToTrade: number
  normalizeOutputs?: boolean
  training: {
    epochs: number
    batchSize: number
    validationSplit: number
  }
}

interface IDeepBotState {
  scaleStats: {
    inputs: IScaleStats[]
    outputs: IScaleStats[]
  }
}

interface IDeepBotJSON {
  config: IDeepBotConfig
  state: IDeepBotState
}

const noActionAdvice = { amountFraction: 0, goalRate: 0 }

export default class DeepBotAdvisor extends BotAdvisor {
  public static fromDir = async (name: string) => {
    const dir = `${settings.dataDir}/deep/${name}`

    // Read config file
    const { config, state }: IDeepBotJSON = JSON.parse((await readFile(`${dir}/config.json`)).toString())

    if (config.watchedData) {
      config.watchedData.forEach(data => {
        data.date = new Date(data.date)
      })
    }

    // Load Model
    const model = new KerasJS.Model({
      filepaths: {
        model: `${dir}/model.json`,
        weights: `${dir}/model_weights.buf`,
        metadata: `${dir}/model_metadata.json`
      },
      filesystem: true
    })
    await model.ready()

    // Instantiate BotAdvisor
    return new DeepBotAdvisor(config, state, model)
  }

  private state: IDeepBotState = {
    scaleStats: { inputs: [], outputs: [] }
  }
  private model: any = null

  constructor(public config: IDeepBotConfig, state?: IDeepBotState, model?: any) {
    super(config)
    if (state) this.state = state
    if (model) this.model = model
  }

  public async shouldOrder(): Promise<IAdvice> {
    if (!this.model) throw new Error('No Model loaded. Please use fromJSON method')
    const exchangeCandlesticks = this.tradingExchange.candlesticks
    const { stoplossPeriod, goalMove, balanceToTrade, lookBack, watchedData } = this.config
    const { scaleStats } = this.state

    // No buy on first candlesticks
    if (exchangeCandlesticks.length < lookBack) {
      return noActionAdvice
    }

    const index = exchangeCandlesticks.length - 1
    const rate = exchangeCandlesticks[index].close

    // Activate network
    const inputs = scale(
      [
        (watchedData ? watchedData[index].inputs : ([] as number[])).concat(
          ...this.watchedExchanges.map(exchange => this.periodToInputs(exchange.candlesticks, index))
        )
      ],
      scaleStats.inputs
    )

    const result = await this.model.predict({
      input: new Float32Array(inputs[0])
    })

    const [outputs] = scale([[...result.output]], scaleStats.outputs, true)
    const [buy, sell] = outputs

    if (stoplossPeriod && goalMove) {
      if (buy > 0.5) {
        // Buy now, sell limit
        return {
          amountFraction: balanceToTrade,
          goalRate: rate * goalMove
        }
      }
    } else {
      if (sell > 0.5) {
        // Sell now
        return {
          amountFraction: -balanceToTrade,
          goalRate: 0
        }
      } else if (buy > 0.5) {
        // Buy now
        return {
          amountFraction: balanceToTrade,
          goalRate: 0
        }
      }
    }

    return noActionAdvice
  }

  public async train(startDate: Date, endDate: Date) {
    const { lookBack, lookForward, normalizeOutputs, watchedData } = this.config
    // Load candlesticks
    const { tradingCandlesticks, watchedCandlesticks } = await this.loadData(startDate, endDate)

    // Align watchedData with candlesticks
    if (watchedData) {
      alignDated([tradingCandlesticks, watchedData])
    }

    const dir = `${settings.dataDir}/deep/bot1`
    if (!(await fileExists(dir))) await mkdir(dir)

    // Prepare Dataset
    const inputs = tradingCandlesticks
      .map((_, i) =>
        (watchedData ? watchedData[i].inputs : ([] as number[])).concat(
          ...watchedCandlesticks.map(c => this.periodToInputs(c, i))
        )
      )
      .slice(lookBack, -lookForward)
    const outputs = tradingCandlesticks
      .map((_, i) => this.periodToOutputs(tradingCandlesticks, i))
      .slice(lookBack, -lookForward)

    // Normalize dataset
    const { dataset: scaledInputs, stats: inputsStats } = scaleStd(inputs)
    const { dataset: scaledOutputs, stats: outputsStats } = normalizeOutputs
      ? scaleStd(outputs)
      : {
          dataset: outputs,
          stats: outputs[0].map(_ => ({ mean: 0, std: 1 }))
        }

    this.state.scaleStats = {
      inputs: inputsStats,
      outputs: outputsStats
    }

    // Save config
    await writeFile(`${dir}/config.json`, JSON.stringify(this.toJSON(), null, 2))

    // Save training data as CSV
    const inputNames = range(0, scaledInputs[0].length).map(n => `Input${n}`)
    const outputNames = range(0, scaledOutputs[0].length).map(n => `Output${n}`)
    const csv = await stringifyCsv([
      ['date', ...inputNames, ...outputNames],
      ...tradingCandlesticks
        .slice(lookBack, -lookForward)
        .map((c, i) => [c.date, ...scaledInputs[i], ...scaledOutputs[i]])
    ])
    await writeFile(`${dir}/train.csv`, csv)

    // Run trainer
    try {
      await runPython(`${__dirname}/train.py`)
    } catch (e) {
      console.error(e.message)
    }

    /*
    // Parse Output CSV
    const data = await readFile(`${dir}/output.csv`)
    const trainOutputs = (await parseCsv(data.toString(), {
      auto_parse: true
    })) as number[][]
    // Remove column name
    trainOutputs.shift()

    return {
      dataset,
      trainOutputs: tradingCandlesticks.map(
        (candlestick, i): IGuess => ({
          date: candlestick.date,
          outputs: trainOutputs[i]
        })
      )
    }
    */
  }

  public toJSON = (): IDeepBotJSON => ({
    config: this.config,
    state: this.state
  })

  private periodToInputs = (candlesticks: ICandlestick[], index: number): number[] => {
    const { lookBack } = this.config
    if (index >= candlesticks.length) return []
    const rate = candlesticks[index].close
    const candles = candlesticks.slice(index + 1 - lookBack, index + 1)

    /*
    // Using indicators
    const halfCandles = candles.slice(-candles.length / 2)
    return [
      indicators.ema(candles.map(c => c.close)),
      indicators.ema(halfCandles.map(c => c.close)),
      indicators.rsi(candles.map(c => c.close)),
      indicators.rsi(halfCandles.map(c => c.close)),
      indicators.mfi(candles),
      indicators.mfi(halfCandles),
      indicators.sma(candles.map(c => c.close)),
      growth(candlesticks[index].high, rate),
      growth(candlesticks[index].low, rate)
    ]
    */

    // Scale volume
    const volumes = candles.map(c => c.volume)
    const volumeMean = mean(volumes)
    const volumeStd = std(volumes) || 1

    return (
      candles
        // Open rate of lookBack candles
        .map(c => growth(c.open, rate))
        // All data of last candles
        .concat(
          ...candles
            .slice(-5)
            .map(c => [growth(c.high, rate), growth(c.low, rate), (c.volume - volumeMean) / volumeStd])
        )
    )
  }

  private periodToOutputs = (candlesticks: ICandlestick[], index: number): number[] => {
    const { lookForward, stoplossPeriod, goalMove } = this.config
    if (index + lookForward >= candlesticks.length) return []
    const rate = candlesticks[index].close
    const nextCandles = candlesticks.slice(index + 1, index + 1 + lookForward)
    const minGrowth = 1.003

    if (stoplossPeriod && goalMove) {
      return [
        // Signal if one of next highs is above goalMove
        nextCandles.some(c => c.high / rate > goalMove) &&
        // and there is no drop after
        nextCandles.every(c => c.close > rate * minGrowth)
          ? 1
          : 0
      ]
    } else {
      return [
        // Buy signal
        nextCandles.every(c => c.close > rate * (goalMove || minGrowth)) ? 1 : 0,
        // Sell signal
        nextCandles.some(c => c.close < rate * minGrowth) ? 1 : 0
      ]
    }
  }
}

// const growth = (n: number, base: number) => (base === 0 ? 0 : (n - base) / base)
const growth = (n: number, base: number) => Math.log(n) - Math.log(base)
