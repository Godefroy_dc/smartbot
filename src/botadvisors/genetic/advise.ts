import { ICandlestick } from '../../interfaces'
import { genesManagers, IGene } from './genes'

interface IGeneticAdvice {
  buy: boolean
  sell: boolean
}

export function advise(genes: IGene[], candlesticks: ICandlestick[]): IGeneticAdvice {
  const buys: boolean[] = []
  const sells: boolean[] = []

  for (const gene of genes) {
    const advice = genesManagers[gene.type].advise(gene, candlesticks)
    if (gene.side === 'buy') {
      buys.push(advice)
    } else {
      sells.push(advice)
    }
  }

  return {
    buy: buys.length !== 0 && buys.every(Boolean),
    sell: sells.length !== 0 && sells.every(Boolean)
  }
}
