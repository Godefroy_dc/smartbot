import { writeFileSync } from 'fs'
import { getRepository } from 'typeorm'
import { GeneticBot } from '../../model/entity/GeneticBot'
import { readFile } from '../../utils'
import BotAdvisor, { IAdvice } from '../BotAdvisor'
import { advise } from './advise'
import evolve from './engine/evolve'
import { getEvolutionFeatures } from './features'
import { lookBack } from './genes'
import { IGeneticBotConfig, IGeneticBotJSON, IGeneticEntity } from './interfaces'

/*
  Bots generated with a Genetic Algorithm
*/

const noActionAdvice = { amountFraction: 0, goalRate: 0 }

export default class GeneticBotAdvisor extends BotAdvisor {
  public static loadFile = async (filepath: string) => {
    // Read config file
    const { config, entity }: IGeneticBotJSON = JSON.parse((await readFile(filepath)).toString())

    // Instantiate BotAdvisor
    return new GeneticBotAdvisor(config, entity)
  }

  public static saveFile(filepath: string, json: IGeneticBotJSON) {
    writeFileSync(filepath, JSON.stringify(json, null, 2))
  }

  public lookBack = 0

  private internalEntity: IGeneticEntity = {
    genes: []
  }

  public get entity() {
    return this.internalEntity
  }

  public set entity(entity) {
    this.internalEntity = entity
    this.lookBack = lookBack(entity.genes)
  }

  constructor(public config: IGeneticBotConfig, entity?: IGeneticEntity) {
    super(config)
    if (entity) this.entity = entity
  }

  public async shouldOrder(): Promise<IAdvice> {
    const { genes } = this.entity
    const { candlesticks } = this.tradingExchange
    const { goalMove } = this.config

    if (genes.length === 0) throw new Error('No genes loaded.')
    if (candlesticks.length < this.lookBack) return noActionAdvice

    const { buy, sell } = advise(genes, candlesticks)
    const rate = candlesticks[candlesticks.length - 1].close

    if (sell) {
      return {
        amountFraction: -0.99999,
        goalRate: 0
      }
    } else if (buy) {
      return {
        amountFraction: 0.99999,
        goalRate: goalMove ? rate * goalMove : 0
      }
    }

    return noActionAdvice
  }

  public async train() {
    // Load candlesticks
    const { tradingCandlesticks, watchedCandlesticks } = await this.loadData(this.config.startDate, this.config.endDate)

    const population = await evolve(
      this.config.genetic,
      getEvolutionFeatures(this.config, tradingCandlesticks, watchedCandlesticks)
    )

    // Save in Database
    await getRepository(GeneticBot).save({
      config: this.config,
      population
    })
  }
}
