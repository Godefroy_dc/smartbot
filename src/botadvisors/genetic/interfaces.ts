import { IBotConfig } from '../BotAdvisor'
import { IConfiguration } from './engine'
import { IGene } from './genes'

export interface IGeneticBotJSON {
  config: IGeneticBotConfig
  entity: IGeneticEntity
}

export interface IGeneticBotConfig extends IBotConfig {
  startDate: Date
  endDate: Date
  genetic: Partial<IConfiguration>
  genes: IGenesConfig
  // Fraction at the end of the total time period to backtest and show result(not used in fitness)
  validationSplit?: number
  // Stop backtest if max drawdown exceeds this value (optimization)
  breakBacktestMaxDrawdown: number
}

export interface IGenesConfig {
  types: string[]
  minBuy: number
  maxBuy: number
  minSell: number
  maxSell: number
}

export interface IGeneticEntity {
  genes: IGene[]
}

export interface IGeneticFitness {
  score: number
  profit: number
  trades: number
  exposure: number
  maxDrawdown: number
}
