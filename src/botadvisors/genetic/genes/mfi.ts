import { IGene, IGeneManager, IGeneMutators, mutateGenePart } from '.'
import * as indicators from '../../../indicators'
import { IOrderSide } from '../../../interfaces'

// Money Flow Index

interface IMFIGene extends IGene {
  type: 'mfi'
  A: number // [2, 100]
  K: number // [-1, 1]
}

const mutators: IGeneMutators<IMFIGene> = {
  A: value => Math.min(Math.max(value + Math.floor((Math.random() - 0.5) * 20), 2), 100),
  K: value => Math.min(Math.max(value + (Math.random() - 0.5) * 0.1, -1), 1)
}

const mfi: IGeneManager<IMFIGene> = {
  advise(gene, candlesticks) {
    if (candlesticks.length < gene.A) return false
    const mfiValue = indicators.mfi(candlesticks.slice(-gene.A))
    return gene.K > 0 ? mfiValue > gene.K : mfiValue < -gene.K
  },
  seed(side: IOrderSide) {
    return {
      type: 'mfi',
      side,
      A: Math.floor(50 * Math.random()) + 2,
      K: Math.random() * 2 - 1
    }
  },
  mutate(gene) {
    return mutateGenePart(gene, mutators)
  },
  lookBack(gene) {
    return gene.A
  }
}

export default mfi
