import { IGene, IGeneManager, IGeneMutators, mutateGenePart } from '.'
import * as indicators from '../../../indicators'
import { IOrderSide } from '../../../interfaces'

interface IEMACrossGene extends IGene {
  type: 'emaCross'
  A: number // [1, 50]
  B: number // [1, 50]
  R: boolean // Inverse equation or not
}

const mutators: IGeneMutators<IEMACrossGene> = {
  A: value => Math.min(Math.max(value + Math.floor((Math.random() - 0.5) * 10), 1), 50),
  B: value => Math.min(Math.max(value + Math.floor((Math.random() - 0.5) * 10), 1), 50)
}

const emaCross: IGeneManager<IEMACrossGene> = {
  advise(gene, candlesticks) {
    if (candlesticks.length < this.lookBack(gene)) return false
    const A = Math.min(gene.A, gene.B)
    const B = Math.max(gene.A, gene.B)
    const emaA = indicators.ema(candlesticks.slice(-A).map(_ => _.close))
    const emaB = indicators.ema(candlesticks.slice(-B).map(_ => _.close))
    const exEmaA = indicators.ema(candlesticks.slice(-A - 1, -1).map(_ => _.close))
    const exEmaB = indicators.ema(candlesticks.slice(-B - 1, -1).map(_ => _.close))
    // Signal when the two EMA lines are crossing
    return gene.R ? emaA < emaB && exEmaA > exEmaB : emaA > emaB && exEmaA < exEmaB
  },
  seed(side: IOrderSide) {
    return {
      type: 'emaCross',
      side,
      A: Math.floor(50 * Math.random()) + 1,
      B: Math.floor(50 * Math.random()) + 1,
      R: Math.random() < 0.5
    }
  },
  mutate(gene) {
    return mutateGenePart(gene, mutators)
  },
  lookBack(gene) {
    return Math.max(gene.A, gene.B) + 1
  }
}

export default emaCross
