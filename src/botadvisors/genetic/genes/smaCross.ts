import { IGene, IGeneManager, IGeneMutators, mutateGenePart } from '.'
import * as indicators from '../../../indicators'
import { IOrderSide } from '../../../interfaces'

interface ISMACrossGene extends IGene {
  type: 'smaCross'
  A: number // [1, 50]
  B: number // [1, 50]
  R: boolean // Inverse equation or not
}

const mutators: IGeneMutators<ISMACrossGene> = {
  A: value => Math.min(Math.max(value + Math.floor((Math.random() - 0.5) * 10), 1), 50),
  B: value => Math.min(Math.max(value + Math.floor((Math.random() - 0.5) * 10), 1), 50)
}

const smaCross: IGeneManager<ISMACrossGene> = {
  advise(gene, candlesticks) {
    if (candlesticks.length < this.lookBack(gene)) return false
    const A = Math.min(gene.A, gene.B)
    const B = Math.max(gene.A, gene.B)
    const smaA = indicators.sma(candlesticks.slice(-A).map(_ => _.close))
    const smaB = indicators.sma(candlesticks.slice(-B).map(_ => _.close))
    const exEmaA = indicators.sma(candlesticks.slice(-A - 1, -1).map(_ => _.close))
    const exEmaB = indicators.sma(candlesticks.slice(-B - 1, -1).map(_ => _.close))
    // Signal when the two SMA lines are crossing
    return gene.R ? smaA < smaB && exEmaA > exEmaB : smaA > smaB && exEmaA < exEmaB
  },
  seed(side: IOrderSide) {
    return {
      type: 'smaCross',
      side,
      A: Math.floor(50 * Math.random()) + 1,
      B: Math.floor(50 * Math.random()) + 1,
      R: Math.random() < 0.5
    }
  },
  mutate(gene) {
    return mutateGenePart(gene, mutators)
  },
  lookBack(gene) {
    return Math.max(gene.A, gene.B) + 1
  }
}

export default smaCross
