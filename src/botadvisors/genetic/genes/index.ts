import { ICandlestick, IOrderSide } from '../../../interfaces'
import ema from './ema'
import emaCross from './emaCross'
import mfi from './mfi'
import momentum from './momentum'
import rsi from './rsi'
import rsiVolume from './rsiVolume'
import smaCross from './smaCross'

export interface IGene {
  type: keyof typeof genesManagers
  side: IOrderSide
}

type IGeneAdvisor<TGene extends IGene> = (gene: TGene, candlesticks: ICandlestick[]) => boolean

export interface IGeneManager<TGene extends IGene> {
  // Order to buy/sell based on last candlesticks
  advise: IGeneAdvisor<TGene>
  // Generate a new random gene
  seed(side: IOrderSide): TGene
  // Randomly mutate an existing gene
  mutate(gene: TGene): TGene
  // Returns the number of candlesticks that are needed
  lookBack(gene: TGene): number
}

export type IGeneMutators<TGene extends IGene> = { [key in keyof TGene]?: (value: TGene[key]) => TGene[key] }

// Mutate only one part of one gene
export function mutateGenePart<TGene extends IGene>(gene: TGene, mutators: IGeneMutators<TGene>): TGene {
  const keys = Object.keys(mutators)
  const index = Math.floor(Math.random() * keys.length)
  const key = keys[index]
  return { ...gene, [key]: mutators[key](gene[key]) }
}

export function lookBack(genes: IGene[]) {
  return Math.max(...genes.map(gene => genesManagers[gene.type].lookBack(gene)))
}

// Available genes
export const genesManagers: Record<string, IGeneManager<IGene>> = {
  ema,
  rsi,
  rsiVolume,
  mfi,
  emaCross,
  smaCross,
  momentum
}
