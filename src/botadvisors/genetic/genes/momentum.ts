import { IGene, IGeneManager, IGeneMutators, mutateGenePart } from '.'
import { IOrderSide } from '../../../interfaces'

interface IMomentumGene extends IGene {
  type: 'momentum'
  A: number // [1, 100]
  K: number // [-1, 1]
  R: boolean // Inverse equation or not
}

const mutators: IGeneMutators<IMomentumGene> = {
  A: value => Math.min(Math.max(value + Math.floor((Math.random() - 0.5) * 20), 1), 100),
  K: value => Math.min(Math.max(value + (Math.random() - 0.5) * 0.1, -1), 1)
}

const momentum: IGeneManager<IMomentumGene> = {
  advise(gene, candlesticks) {
    if (candlesticks.length < gene.A + 1) return false
    const rate = candlesticks[candlesticks.length - 1].close
    const prevRate = candlesticks[candlesticks.length - gene.A - 1].close
    const ratio = ((rate - prevRate) / prevRate) * 10
    return gene.R ? ratio > gene.K : ratio < gene.K
  },
  seed(side: IOrderSide) {
    return {
      type: 'momentum',
      side,
      A: Math.floor(50 * Math.random()) + 1,
      K: Math.random() * 2 - 1,
      R: Math.random() < 0.5
    }
  },
  mutate(gene) {
    return mutateGenePart(gene, mutators)
  },
  lookBack(gene) {
    return gene.A
  }
}

export default momentum
