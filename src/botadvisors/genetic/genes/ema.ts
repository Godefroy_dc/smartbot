import { IGene, IGeneManager, IGeneMutators, mutateGenePart } from '.'
import * as indicators from '../../../indicators'
import { IOrderSide } from '../../../interfaces'

interface IEMAGene extends IGene {
  type: 'ema'
  A: number // [2, 100]
  K: number // [-1, 1]
  R: boolean // Inverse equation or not
}

const mutators: IGeneMutators<IEMAGene> = {
  A: value => Math.min(Math.max(value + Math.floor((Math.random() - 0.5) * 20), 2), 100),
  K: value => Math.min(Math.max(value + (Math.random() - 0.5) * 0.1, -1), 1)
  // R: () => Math.random() < 0.5
}

const ema: IGeneManager<IEMAGene> = {
  advise(gene, candlesticks) {
    if (candlesticks.length < gene.A) return false
    const rate = candlesticks[candlesticks.length - 1].close
    const currentEMA = indicators.ema(candlesticks.slice(-gene.A).map(_ => _.close))
    // Compare EMA and price with a 10 multiplier to have a ratio approximately between -1 and 1
    const ratio = ((currentEMA - rate) / rate) * 10
    return gene.R ? ratio > gene.K : ratio < gene.K
  },
  seed(side: IOrderSide) {
    return {
      type: 'ema',
      side,
      A: Math.floor(50 * Math.random()) + 2,
      K: Math.random() * 2 - 1,
      R: Math.random() < 0.5
    }
  },
  mutate(gene) {
    return mutateGenePart(gene, mutators)
  },
  lookBack(gene) {
    return gene.A
  }
}

export default ema
