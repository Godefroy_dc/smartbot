import { IGene, IGeneManager, IGeneMutators, mutateGenePart } from '.'
import * as indicators from '../../../indicators'
import { IOrderSide } from '../../../interfaces'

interface IRSIVolumeGene extends IGene {
  type: 'rsiVolume'
  A: number // [2, 100]
  K: number // [-1, 1]
}

const mutators: IGeneMutators<IRSIVolumeGene> = {
  A: value => Math.min(Math.max(value + Math.floor((Math.random() - 0.5) * 20), 2), 100),
  K: value => Math.min(Math.max(value + (Math.random() - 0.5) * 0.1, -1), 1)
}

const rsiVolume: IGeneManager<IRSIVolumeGene> = {
  advise(gene, candlesticks) {
    if (candlesticks.length < gene.A) return false
    const rsiValue = indicators.rsi(candlesticks.slice(-gene.A).map(_ => _.volume))
    return gene.K > 0 ? rsiValue > gene.K : rsiValue < -gene.K
  },
  seed(side: IOrderSide) {
    return {
      type: 'rsiVolume',
      side,
      A: Math.floor(50 * Math.random()) + 2,
      K: Math.random() * 2 - 1
    }
  },
  mutate(gene) {
    return mutateGenePart(gene, mutators)
  },
  lookBack(gene) {
    return gene.A
  }
}

export default rsiVolume
