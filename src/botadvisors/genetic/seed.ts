import { IOrderSide } from '../../interfaces'
import { genesManagers, IGene } from './genes'
import { IGenesConfig, IGeneticEntity } from './interfaces'

export function seed(config: IGenesConfig): IGeneticEntity {
  return {
    genes: [
      ...Array.from(Array(config.minBuy)).map(() => seedGene(config, 'buy')),
      ...Array.from(Array(config.minSell)).map(() => seedGene(config, 'sell'))
    ]
  }
}

export function seedGene(config: IGenesConfig, side?: IOrderSide): IGene {
  const name = config.types[Math.floor(Math.random() * config.types.length)]
  if (!side) side = Math.random() < 0.5 ? 'buy' : 'sell'
  return genesManagers[name].seed(side)
}
