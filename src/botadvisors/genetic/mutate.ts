import { genesManagers } from './genes'
import { IGeneticEntity } from './interfaces'

export function mutate(entity: IGeneticEntity): IGeneticEntity {
  const i = Math.floor(Math.random() * entity.genes.length)
  const genes = [...entity.genes]
  genes[i] = genesManagers[genes[i].type].mutate(genes[i])
  return { genes }
}
