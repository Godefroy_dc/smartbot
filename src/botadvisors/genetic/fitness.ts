import Bot from '../../bots/Bot'
import { Candlestick } from '../../model/entity/Candlestick'
import GeneticBotAdvisor from './GeneticBotAdvisor'
import { IGeneticBotConfig, IGeneticEntity, IGeneticFitness } from './interfaces'

export async function fitness(
  entity: IGeneticEntity,
  config: IGeneticBotConfig,
  tradingCandlesticks: Candlestick[],
  watchedCandlesticks: Candlestick[][]
): Promise<IGeneticFitness> {
  const botadvisor = new GeneticBotAdvisor(config, entity)
  const bot = new Bot(botadvisor)

  const exchange = bot.tradingExchange
  const initialBalances: [number, number] = [exchange.account1.balance, exchange.account2.balance]

  const stopMaxDrawdown = () => bot.maxDrawdown >= botadvisor.config.breakBacktestMaxDrawdown

  // Start Backtest
  bot.start()
  await botadvisor.backtestCandlesticks(tradingCandlesticks, watchedCandlesticks, stopMaxDrawdown)
  bot.stop()

  const lastCandlestick = exchange.candlesticks[exchange.candlesticks.length - 1]
  const startingBalance = initialBalances[0] + initialBalances[1] * exchange.candlesticks[0].open
  const endingBalance = exchange.account1.balance + exchange.account2.balance * lastCandlestick.close
  const profit = (endingBalance - startingBalance) / startingBalance
  const exposure = bot.getExposure()
  const maxDrawdown = bot.maxDrawdown

  // Fitness score
  const score =
    stopMaxDrawdown() || exchange.trades.length < 2
      ? -Infinity
      : profit * // Balance growth
          (1 - maxDrawdown) - // Weight with max drawdown
        0.000000001 * entity.genes.length // Prefer less genes

  return {
    score,
    profit,
    trades: exchange.trades.length,
    exposure,
    maxDrawdown
  }
}
