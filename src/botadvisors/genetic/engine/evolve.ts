// Genetic algorithm based on https://github.com/Glavin001/genetic-js

import { PairWiseSelection, SingleSelection } from './Selection'

export interface IConfiguration {
  iterations: number
  size: number
  fittestAlwaysSurvives: boolean
  mutation: number
  crossover: number
  skipNotifications: number
}

interface INotification<Entity, Fitness extends IFitness> {
  population: Population<Entity, Fitness>
  generation: number
}

interface IState<Entity, Fitness extends IFitness> {
  population: Population<Entity, Fitness>
  generation: number
}

export interface IFeatures<Entity, Fitness extends IFitness> {
  select1: SingleSelection<Entity, Fitness>
  select2?: PairWiseSelection<Entity, Fitness>
  seed(): Entity
  mutate(entity: Entity): Entity
  crossover?(mother: Entity, father: Entity): Entity[]
  fitness(entity: Entity): Fitness | Promise<Fitness>
  shouldContinue?(state: IState<Entity, Fitness>): boolean
  notification?(notification: INotification<Entity, Fitness>): void | Promise<void>
}

export type Population<Entity, Fitness extends IFitness> = Array<{
  entity: Entity
  fitness: Fitness
}>

export interface IFitness {
  score: number
}

const defaultConfig: IConfiguration = {
  iterations: 100,
  size: 20,
  fittestAlwaysSurvives: true,
  mutation: 0.2,
  crossover: 0.3,
  skipNotifications: 0
}

export default async function evolve<Entity, Fitness extends IFitness>(
  partialConfig: Partial<IConfiguration> = {},
  features: IFeatures<Entity, Fitness>
): Promise<Population<Entity, Fitness>> {
  const config: IConfiguration = { ...defaultConfig, ...partialConfig }

  let population: Population<Entity, Fitness> = []

  function mutateOrNot(entity: Entity) {
    // applies mutation based on mutation probability
    return Math.random() <= config.mutation && features.mutate ? features.mutate(entity) : entity
  }

  async function addEntity(pop: Population<Entity, Fitness>, entity: Entity) {
    const fitness = await features.fitness(entity)
    pop.push({ entity, fitness })
  }

  // seed the population
  for (let currSeed = 0; currSeed < config.size; currSeed++) {
    await addEntity(population, features.seed())
  }

  for (let generation = 1; generation < config.iterations + 1; generation++) {
    // crossover and mutate
    const newPopulation: Population<Entity, Fitness> = []

    // Keep the best entity
    if (config.fittestAlwaysSurvives) {
      newPopulation.push(population[0])
    }

    while (newPopulation.length < config.size) {
      if (
        features.crossover &&
        features.select2 && // if there is a crossover function
        Math.random() <= config.crossover && // base crossover on specified probability
        newPopulation.length + 1 < config.size // keeps us from going 1 over the max population size
      ) {
        const parents = features.select2(population)
        await Promise.all(
          features
            .crossover(parents[0], parents[1])
            .map(mutateOrNot)
            .map(entity => addEntity(newPopulation, entity))
        )
      } else {
        await addEntity(newPopulation, mutateOrNot(features.select1(population)))
      }
    }

    // Sort population by fitness
    population = newPopulation.sort((a, b) => (a.fitness.score > b.fitness.score ? -1 : 1))

    // Notifications
    const shouldSendNotification =
      config.skipNotifications === 0 || generation % config.skipNotifications === 0 || generation === config.iterations
    if (shouldSendNotification && features.notification) {
      await features.notification({
        generation,
        population
      })
    }

    // Custom break
    const shouldContinue =
      !features.shouldContinue ||
      features.shouldContinue({
        generation,
        population
      })
    if (!shouldContinue) break
  }

  return population
}
