import { IFitness, Population } from './evolve'

export type SingleSelection<Entity, Fitness extends IFitness> = (pop: Population<Entity, Fitness>) => Entity

export type PairWiseSelection<Entity, Fitness extends IFitness> = (pop: Population<Entity, Fitness>) => [Entity, Entity]

export const Select1 = {
  Tournament2<Entity, Fitness extends IFitness>(pop: Population<Entity, Fitness>): Entity {
    const popSize = pop.length
    const firstPick = pop[Math.floor(Math.random() * popSize)]
    const secondPick = pop[Math.floor(Math.random() * popSize)]
    return firstPick.fitness.score > secondPick.fitness.score ? firstPick.entity : secondPick.entity
  },
  Tournament3<Entity, Fitness extends IFitness>(pop: Population<Entity, Fitness>): Entity {
    const popSize = pop.length
    const firstPick = pop[Math.floor(Math.random() * popSize)]
    const secondPick = pop[Math.floor(Math.random() * popSize)]
    const thirdPick = pop[Math.floor(Math.random() * popSize)]
    let best = firstPick.fitness.score > secondPick.fitness.score ? firstPick : secondPick
    best = best.fitness.score > thirdPick.fitness.score ? best : thirdPick
    return best.entity
  },
  Fittest<Entity, Fitness extends IFitness>(pop: Population<Entity, Fitness>): Entity {
    return pop[0].entity
  },
  Random<Entity, Fitness extends IFitness>(pop: Population<Entity, Fitness>): Entity {
    return pop[Math.floor(Math.random() * pop.length)].entity
  }
  /*
  RandomLinearRank<Entity, Fitness extends IFitness>(pop: Population<Entity, Fitness>): Entity {
    this.internalGenState.rlr = this.internalGenState.rlr || 0
    return pop[Math.floor(Math.random() * Math.min(pop.length, this.internalGenState.rlr++))].entity
  },
  Sequential<Entity, Fitness extends IFitness>(pop: Population<Entity, Fitness>): Entity {
    this.internalGenState.seq = this.internalGenState.seq || 0
    return pop[this.internalGenState.seq++ % pop.length].entity
  }
  */
}

export const Select2 = {
  Tournament2<Entity, Fitness extends IFitness>(pop: Population<Entity, Fitness>): [Entity, Entity] {
    return [Select1.Tournament2(pop), Select1.Tournament2(pop)]
  },
  Tournament3<Entity, Fitness extends IFitness>(pop: Population<Entity, Fitness>): [Entity, Entity] {
    return [Select1.Tournament3(pop), Select1.Tournament3(pop)]
  },
  Random<Entity, Fitness extends IFitness>(pop: Population<Entity, Fitness>): [Entity, Entity] {
    return [Select1.Random(pop), Select1.Random(pop)]
  },
  /*
  RandomLinearRank<Entity, Fitness extends IFitness>(pop: Population<Entity, Fitness>): [Entity, Entity] {
    return [Select1.RandomLinearRank(pop), Select1.RandomLinearRank(pop)]
  },
  Sequential<Entity, Fitness extends IFitness>(pop: Population<Entity, Fitness>): [Entity, Entity] {
    return [Select1.Sequential(pop), Select1.Sequential(pop)]
  },
  */
  FittestRandom<Entity, Fitness extends IFitness>(pop: Population<Entity, Fitness>): [Entity, Entity] {
    return [Select1.Fittest(pop), Select1.Random(pop)]
  }
}
