import { IGene } from './genes'
import { IGenesConfig, IGeneticEntity } from './interfaces'
import { seedGene } from './seed'

// Apply a random crossover between a mother and a father
export function crossover(mother: IGeneticEntity, father: IGeneticEntity, config: IGenesConfig): IGeneticEntity[] {
  const names = Object.keys(crossovers)
  const name = names[Math.floor(Math.random() * names.length)]
  return crossovers[name](mother.genes, father.genes, config).map(genes => ({ genes }))
}

type Crossover = (mother: IGene[], father: IGene[], config: IGenesConfig) => IGene[][]

const crossovers: Record<string, Crossover> = {
  // Shuffle genes by side
  mixGenes(mother, father) {
    // Shuffle all genes from mother and father
    const genes = [...mother, ...father].sort(() => Math.random() - 0.5)
    // Get number of mother's genes buy side
    const nMotherBuy = mother.filter(gene => gene.side === 'buy').length
    const nMotherSell = mother.length - nMotherBuy
    // Regroup genes by side
    const buyGenes = genes.filter(gene => gene.side === 'buy')
    const sellGenes = genes.filter(gene => gene.side === 'sell')
    // Procreate son and daughter
    const son = buyGenes.slice(0, nMotherBuy).concat(sellGenes.slice(0, nMotherSell))
    const daughter = buyGenes.slice(nMotherBuy).concat(sellGenes.slice(nMotherSell))
    return [son, daughter]
  },

  // Add new genes to mother and father
  addGenes(mother, father, config) {
    return [addGene(mother, config), addGene(father, config)]
  },

  // Remove random genes from father and mother
  removeGenes(mother, father, config) {
    return [removeGene(mother, config), removeGene(father, config)]
  }
}

function addGene(parent: IGene[], config: IGenesConfig): IGene[] {
  const nBuy = parent.filter(gene => gene.side === 'buy').length
  const nSell = parent.length - nBuy
  if (nBuy < config.maxBuy && nSell < config.maxSell) {
    return parent.concat(seedGene(config))
  }
  if (nBuy < config.maxBuy) {
    return parent.concat(seedGene(config, 'buy'))
  }
  if (nSell < config.maxSell) {
    return parent.concat(seedGene(config, 'sell'))
  }
  return parent
}

function removeGene(parent: IGene[], config: IGenesConfig): IGene[] {
  const buyGenes = parent.filter(gene => gene.side === 'buy')
  const sellGenes = parent.filter(gene => gene.side === 'sell')
  if (buyGenes.length > config.minBuy && sellGenes.length < config.minSell) {
    return parent.splice(Math.floor(Math.random() * parent.length), 1)
  }
  if (buyGenes.length > config.minBuy) {
    return [...buyGenes.splice(Math.floor(Math.random() * buyGenes.length), 1), ...sellGenes]
  }
  if (sellGenes.length < config.minSell) {
    return [...buyGenes, ...sellGenes.splice(Math.floor(Math.random() * buyGenes.length), 1)]
  }
  return parent
}
