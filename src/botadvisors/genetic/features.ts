import chalk from 'chalk'
import { Candlestick } from '../../model/entity/Candlestick'
import settings from '../../settings'
import { crossover } from './crossover'
import { IFeatures, Select1, Select2 } from './engine'
import { fitness } from './fitness'
import { IGene } from './genes'
import GeneticBotAdvisor from './GeneticBotAdvisor'
import { IGeneticBotConfig, IGeneticEntity, IGeneticFitness } from './interfaces'
import { mutate } from './mutate'
import { seed } from './seed'

export function getEvolutionFeatures(
  config: IGeneticBotConfig,
  tradingCandlesticks: Candlestick[],
  watchedCandlesticks: Candlestick[][]
): IFeatures<IGeneticEntity, IGeneticFitness> {
  // Separate train and test periods
  const trainLength = Math.trunc((1 - (config.validationSplit || 0)) * tradingCandlesticks.length)
  const train = {
    tradingCandlesticks: tradingCandlesticks.slice(0, trainLength),
    watchedCandlesticks: watchedCandlesticks.map(c => c.slice(0, trainLength))
  }
  const test = {
    tradingCandlesticks: tradingCandlesticks.slice(trainLength),
    watchedCandlesticks: watchedCandlesticks.map(c => c.slice(trainLength))
  }

  return {
    select1: Select1.Tournament2,
    select2: Select2.Tournament2,

    seed: () => seed(config.genes),
    mutate,
    crossover: (mother, father) => crossover(mother, father, config.genes),
    fitness: entity => fitness(entity, config, train.tradingCandlesticks, train.watchedCandlesticks),

    async notification(notification) {
      const { entity, fitness: trainFitness } = notification.population[0]

      // Log generation and best entity
      console.log(chalk.gray(`# ${notification.generation}`) + '\t' + chalk.blue(summariesEntity(entity)))

      // Log training fitness
      console.log(chalk.cyan(`\tTrain: `) + summariesFitness(trainFitness))

      // Log testing fitness
      if (test.tradingCandlesticks.length !== 0) {
        const testFitness = await fitness(entity, config, test.tradingCandlesticks, test.watchedCandlesticks)
        console.log(chalk.yellow(`\tTest: `) + summariesFitness(testFitness))
      }

      // Empty line
      console.log('')

      // Saving JSON file for tests
      GeneticBotAdvisor.saveFile(`${settings.dataDir}/genetic/bot1.json`, {
        config,
        entity
      })
    }
  }
}

const summariesFitness = (entityFitness: IGeneticFitness) =>
  chalk[entityFitness.profit > 0 ? 'green' : 'red'](`\tProfit: ${Math.round(entityFitness.profit * 100)}%`) +
  `\t${entityFitness.trades} trades` +
  `\tExposure: ${Math.round(entityFitness.exposure * 100)}%` +
  `\tMax drawdown: ${Math.round(entityFitness.maxDrawdown * 100)}%`

const summariesEntity = (entity: IGeneticEntity) =>
  `Best: ${entity.genes.length} genes (${summariesGenes(entity.genes)})`

const summariesGenes = (genes: IGene[]) =>
  Object.entries(
    genes.reduce<Record<string, number>>((acc, gene) => {
      if (!acc[gene.type]) acc[gene.type] = 1
      else acc[gene.type]++
      return acc
    }, {})
  )
    .map(([key, value]) => key + (value === 1 ? '' : `*${value}`))
    .join(', ')
