import CCXTExchange from '../exchanges/CCXTExchange'
import { CandlestickSizes, IExchangePair } from '../interfaces'
import { Candlestick } from '../model/entity/Candlestick'
import settings from '../settings'
import { alignDated, promiseSerial } from '../utils'

export interface IBotConfig {
  tradingExchange: IExchangePair
  watchedExchanges: IExchangePair[]
  candlestickSize: CandlestickSizes
  goalMove?: number
  stoplossPeriod?: number
}

export interface IAdvice {
  amountFraction: number
  goalRate: number
}

export default abstract class BotAdvisor {
  public tradingExchange: CCXTExchange
  public watchedExchanges: CCXTExchange[]

  public constructor(public config: IBotConfig) {
    this.tradingExchange = new CCXTExchange(config.tradingExchange, config.candlestickSize)
    this.watchedExchanges = config.watchedExchanges.map(e => new CCXTExchange(e, config.candlestickSize))
  }

  public abstract async shouldOrder(): Promise<IAdvice>

  public async backtest(startDate: Date, endDate: Date, lookBack = 0) {
    const { tradingCandlesticks, watchedCandlesticks } = await this.loadData(startDate, endDate, lookBack)
    await this.backtestCandlesticks(tradingCandlesticks, watchedCandlesticks)
  }

  public async backtestCandlesticks(
    tradingCandlesticks: Candlestick[],
    watchedCandlesticks: Candlestick[][],
    stop?: () => boolean
  ) {
    const watchedData = this.config['watchedData']
    if (watchedData) {
      alignDated([tradingCandlesticks, watchedData])
    }

    // Play each candlestick and get trades
    const candlesCount = tradingCandlesticks.length
    const watchedExchangesCount = this.watchedExchanges.length

    for (let ci = 0; ci < candlesCount; ci++) {
      await this.tradingExchange.addCandlestick(tradingCandlesticks[ci])
      for (let ei = 0; ei < watchedExchangesCount; ei++) {
        await this.watchedExchanges[ei].addCandlestick(watchedCandlesticks[ei][ci])
      }
      if (stop && stop()) break
    }
  }

  protected async loadData(startDate: Date, endDate: Date, lookBack = 0) {
    // Add previous candlesticks, needed for computations
    if (lookBack) {
      startDate = new Date(
        startDate.getTime() -
          lookBack * settings.candlestickSizes[CandlestickSizes[this.tradingExchange.candlestickSize]]
      )
    }

    const tradingCandlesticks = await this.tradingExchange.getAndSaveCandlesticks(startDate, endDate)

    const watchedCandlesticks = await promiseSerial(
      this.watchedExchanges.map(exchange => async () => {
        // Prevent from loading tradingExchange two times
        if (
          exchange.exchangePair.exchange === this.tradingExchange.exchangePair.exchange &&
          exchange.exchangePair.currency1 === this.tradingExchange.exchangePair.currency1 &&
          exchange.exchangePair.currency2 === this.tradingExchange.exchangePair.currency2
        ) {
          return tradingCandlesticks
        }
        return exchange.getAndSaveCandlesticks(startDate, endDate)
      })
    )

    // Align candlesticks
    alignDated([tradingCandlesticks, ...watchedCandlesticks])

    if (watchedCandlesticks.some(c => c.length !== tradingCandlesticks.length)) {
      throw new Error('Loaded candlesticks are not alined')
    }

    return { tradingCandlesticks, watchedCandlesticks }
  }
}
