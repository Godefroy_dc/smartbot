import BotAdvisor, { IBotConfig } from './BotAdvisor'

export interface IClassicBotConfig extends IBotConfig {
  balanceToTrade: number
}

const noActionAdvice = { amountFraction: 0, goalRate: 0 }

export default class ClassicBotAdvisor extends BotAdvisor {
  constructor(public config: IClassicBotConfig) {
    super(config)
  }

  public async shouldOrder() {
    const candlesticks = this.tradingExchange.candlesticks
    const { balanceToTrade } = this.config
    if (candlesticks.length < 2) return noActionAdvice

    const lastCandle = candlesticks[candlesticks.length - 1]
    const rate = lastCandle.close

    if (rate < candlesticks[candlesticks.length - 2].close * 0.96) {
      return {
        amountFraction: balanceToTrade,
        goalRate: rate * 1.02
      }
    }

    /* Momentum, doesn't beat market but not bad at all
    if (rate > candlesticks[candlesticks.length - 2].close) {
      return {
        amountFraction: balanceToTrade,
        goalRate: 0
      }
    } else {
      return {
        amountFraction: -balanceToTrade,
        goalRate: 0
      }
    }
    */

    // Don't buy
    return noActionAdvice
  }
}
