import { CandlestickSizes, IAccount, ICandlestick, IExchangePair, IOrder, IOrderSide, ITrade } from '../interfaces'
import settings from '../settings'
import EventPromiseEmitter from './EventPromiseEmitter'

export default abstract class Exchange extends EventPromiseEmitter {
  public minTradeAmount = 0.0025

  public fees = {
    maker: 0,
    taker: 0
  }

  public account1: IAccount
  public account2: IAccount

  public candlesticks: ICandlestick[] = []
  public trades: ITrade[] = []
  public orders: IOrder[] = []

  constructor(public exchangePair: IExchangePair, public candlestickSize: CandlestickSizes) {
    super()
    this.account1 = {
      symbol: exchangePair.currency1,
      balance: 0,
      availableBalance: 0
    }
    this.account2 = {
      symbol: exchangePair.currency2,
      balance: 0,
      availableBalance: 0
    }
  }

  // Buy amount of symbol2 at rate (rate2 / rate1)
  // If rate = null, buy at market rate
  // Promise resolve with trade when the order has been filled
  public abstract async order(type: IOrderSide, amount: number, rate?: number): Promise<IOrder>

  public abstract async cancelOrder(order: IOrder): Promise<void>

  public getFeesRatio(market: boolean) {
    return 1 - (market ? this.fees.taker : this.fees.maker) / 100
  }

  public async addCandlestick(candlestick: ICandlestick) {
    this.candlesticks.push(candlestick)
    // Emit candlestick
    await this.emit('candlestick', this)
  }

  public clearFirstCandlesticks(n: number) {
    this.candlesticks.splice(0, n)
  }

  public getTradeBalancesDiff(trade: ITrade) {
    const balance1Diff =
      trade.side === 'buy'
        ? -trade.amount * trade.rate
        : trade.amount * trade.rate * this.getFeesRatio(trade.rate === null)

    const balance2Diff = trade.side === 'buy' ? trade.amount * this.getFeesRatio(trade.rate === null) : -trade.amount

    return [balance1Diff, balance2Diff]
  }

  // Market rate is last candlestick close rate
  public getMarketRate() {
    if (this.candlesticks.length === 0) {
      throw new Error('Last candlestick needed to buy at market rate on FakeExchange')
    }
    return this.candlesticks[this.candlesticks.length - 1].close
  }

  protected addOrder(order: IOrder, impactBalance: boolean = false) {
    if (settings.verbose) {
      console.log(
        `${order.date.toLocaleString()} - Order ${order.side}, amount: ${order.amount}, rate: ${
          order.rate
        }, balance1: ${this.account1.balance}, balance2: ${this.account2.balance}, availableBalance1: ${
          this.account1.availableBalance
        }, availableBalance2: ${this.account2.availableBalance}`
      )
    }

    this.orders.push(order)

    // Impact available balances
    if (impactBalance) {
      if (order.side === 'buy') {
        this.account1.availableBalance -= order.amount * order.rate
      } else if (order.side === 'sell') {
        this.account2.availableBalance -= order.amount
      }
    }
  }

  protected removeOrder(order: IOrder, impactBalance: boolean = false) {
    const i = this.orders.indexOf(order)
    if (i === -1) return console.warn('Tries to remove an order that is already removed')
    this.orders.splice(i, 1)

    // Impact available balance
    if (impactBalance) {
      if (order.side === 'buy') {
        this.account1.availableBalance += order.amount * order.rate
      } else if (order.side === 'sell') {
        this.account2.availableBalance += order.amount
      }
    }
  }

  protected applyTrade(trade: ITrade, order: IOrder, impactBalance: boolean = false) {
    if (impactBalance) {
      const [balance1Diff, balance2Diff] = this.getTradeBalancesDiff(trade)

      // Impact balances
      this.account1.balance += balance1Diff
      this.account2.balance += balance2Diff

      // Impact available balances
      this.account1.availableBalance += balance1Diff
      this.account2.availableBalance += balance2Diff
    }

    if (settings.verbose) {
      console.log(
        `${trade.date.toLocaleString()} - Trade ${trade.side}, amount: ${trade.amount}, rate: ${
          trade.rate
        }, balance1: ${this.account1.balance}, balance2: ${this.account2.balance}, availableBalance1: ${
          this.account1.availableBalance
        }, availableBalance2: ${this.account2.availableBalance}`
      )
    }

    // Emit trade
    this.trades.push(trade)
    order.trades.push(trade)
    this.emit('trade', trade).catch(e => console.error(e))
    order.events.emit('trade', trade)
  }

  protected getTradeConfig() {
    return {
      currency1: this.account1.symbol,
      currency2: this.account2.symbol
    }
  }
}
