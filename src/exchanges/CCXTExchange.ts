import * as ccxt from 'ccxt'
import { EventEmitter } from 'events'
import { getCustomRepository } from '../database'
import { CandlestickSizes, ICandlestick, IExchangePair, IOrder, IOrderSide } from '../interfaces'
import { Candlestick } from '../model/entity/Candlestick'
import { CandlestickRepository } from '../model/repository/CandlestickRepository'
import settings from '../settings'
import { slackPostMessage } from '../utils'
import Exchange from './Exchange'

const sizeToTimeframe = {
  [CandlestickSizes.min1]: '1m',
  [CandlestickSizes.min5]: '5m',
  [CandlestickSizes.min15]: '15m',
  [CandlestickSizes.min30]: '30m',
  [CandlestickSizes.h1]: '1h',
  [CandlestickSizes.h3]: '3h',
  [CandlestickSizes.h6]: '6h',
  [CandlestickSizes.h12]: '12h',
  [CandlestickSizes.d1]: '1d'
}

const instances: ccxt.Exchange[] = []

export default class CCXTExchange extends Exchange {
  public fees = {
    maker: 0.15,
    taker: 0.25
  }

  private exchange: ccxt.Exchange
  private symbol: string
  private checkTimeout?: NodeJS.Timer

  constructor(exchangePair: IExchangePair, candlestickSize: CandlestickSizes) {
    super(exchangePair, candlestickSize)
    this.symbol = `${this.account2.symbol}/${this.account1.symbol}`

    const exchangeName = exchangePair.exchange

    // Try to get existing instance
    const exchange = instances.find(instance => instance.id === exchangeName)
    if (exchange) {
      this.exchange = exchange
    } else {
      const exchangeSettings = settings.exchanges.find(e => e.name === exchangeName)
      const exchangeClass: typeof ccxt.Exchange = ccxt[exchangeName]

      if (!exchangeSettings) throw new Error(`No settings for exchange: ${exchangeName}`)
      if (!exchangeClass) throw new Error(`No ccxt interface for exchange: ${exchangeName}`)

      // Instantiate
      this.exchange = new exchangeClass({
        apiKey: exchangeSettings.apiKey,
        secret: exchangeSettings.apiSecret,
        rateLimit: 5000,
        enableRateLimit: true
      })
      instances.push(this.exchange)
    }
  }

  public async watch() {
    if (this.checkTimeout !== undefined) return

    await this.loadBalances()
    await this.loadOrders()
    await this.loadLastCandles()

    this.intervalCheck(5000)
  }

  public stop() {
    if (this.checkTimeout !== undefined) {
      clearTimeout(this.checkTimeout)
      this.checkTimeout = undefined
    }
  }

  public async order(side: IOrderSide, amount: number, rate?: number): Promise<IOrder> {
    const type = typeof rate === 'undefined' ? 'market' : 'limit'

    try {
      const ccxtOrder: ccxt.Order = await this.exchange.createOrder(
        this.symbol,
        type,
        side,
        this.exchange.amountToPrecision(this.symbol, amount),
        rate && this.exchange.priceToPrecision(this.symbol, rate)
      )

      // Register order
      const order: IOrder = {
        ...this.getTradeConfig(),
        id: ccxtOrder.id,
        type: ccxtOrder.type,
        side: ccxtOrder.side,
        date: new Date(ccxtOrder.datetime),
        rate: ccxtOrder.price,
        amount: ccxtOrder.amount,
        trades: [],
        events: new EventEmitter()
      }
      this.addOrder(order)

      return order
    } catch (e) {
      console.error('Error creating Order:', e)
      throw e
    }
  }

  public async cancelOrder(order: IOrder): Promise<void> {
    try {
      await this.exchange.cancelOrder(order.id)
      this.removeOrder(order)
      order.events.emit('cancel')
    } catch (error) {
      if (error instanceof ccxt.OrderNotFound) {
        console.log('Cancel Order error (not found): ', order.id, `(rate: ${order.rate}, amount: ${order.amount})`)
      } else {
        throw error
      }
    }
  }

  public async getAndSaveCandlesticks(startDate: Date, endDate: Date): Promise<Candlestick[]> {
    const repository = await getCustomRepository<CandlestickRepository, Candlestick>(CandlestickRepository)

    const lastCandlestick = await repository.getLastCandlestick(this.exchangePair, this.candlestickSize)

    // No saved candle, get whole range
    if (!lastCandlestick) {
      const candles = await this.getCandles(startDate, endDate)
      if (candles.length !== 0) await repository.saveCandles(this.exchangePair, candles)
    } else {
      // Get candles after last saved
      if (lastCandlestick.date < endDate) {
        const candles = await this.getCandles(lastCandlestick.date, endDate)
        if (candles.length !== 0) await repository.saveCandles(this.exchangePair, candles)
      }
      // Get candles before first saved
      const firstCandlestick = await repository.getFirstCandlestick(this.exchangePair, this.candlestickSize)
      if (firstCandlestick && firstCandlestick.date > startDate) {
        const candles = await this.getCandles(startDate, firstCandlestick.date)
        if (candles.length !== 0) await repository.saveCandles(this.exchangePair, candles)
      }
    }

    return repository.getCandlesticks(this.exchangePair, this.candlestickSize, startDate, endDate)
  }

  private async loadBalances() {
    const balances = await this.exchange.fetchBalance()

    const balance1: ccxt.Balance = balances[this.account1.symbol] || { total: 0, free: 0 }
    const balance2: ccxt.Balance = balances[this.account2.symbol] || { total: 0, free: 0 }
    this.account1.balance = balance1.total
    this.account1.availableBalance = balance1.free
    this.account2.balance = balance2.total
    this.account2.availableBalance = balance2.free
    if (settings.verbose) {
      console.log(
        `Wallet ${this.account1.symbol}: ${this.account1.balance} (available: ${this.account1.availableBalance})`
      )
      console.log(
        `Wallet ${this.account2.symbol}: ${this.account2.balance} (available: ${this.account2.availableBalance})`
      )
      await slackPostMessage(
        `Wallet ${this.account1.symbol}: ${this.account1.balance} (available: ${this.account1.availableBalance})`
      )
      await slackPostMessage(
        `Wallet ${this.account2.symbol}: ${this.account2.balance} (available: ${this.account2.availableBalance})`
      )
    }
  }

  private async loadOrders() {
    const orders = await this.exchange.fetchOpenOrders(this.symbol)

    this.orders = orders.map(
      (o): IOrder => ({
        ...this.getTradeConfig(),
        id: o.id,
        type: o.type,
        side: o.side,
        date: new Date(o.datetime),
        rate: o.price,
        amount: Math.abs(o.amount),
        trades: [],
        events: new EventEmitter()
      })
    )
    if (settings.verbose) console.log('Orders loaded:', this.orders.length)
  }

  // TODO: add "since" parameter to fetchOHLCV to ensure same behaviour on all exchanges
  private async loadLastCandles() {
    if (!this.exchange.fetchOHLCV) throw new Error(`No fetchOHLCV on exchange ${this.exchange.name}`)

    const candles = await this.exchange.fetchOHLCV(this.symbol, sizeToTimeframe[this.candlestickSize])

    this.candlesticks = this.parseCandles(candles)

    if (settings.verbose) {
      console.log(
        'Candlesticks loaded:',
        this.candlesticks[0].date.toLocaleString(),
        '-',
        this.candlesticks[this.candlesticks.length - 1].date.toLocaleString(),
        `(${this.candlesticks.length}, last close: ${this.candlesticks[this.candlesticks.length - 1].close})`
      )
    }
  }

  private async getCandles(startDate: Date, endDate: Date): Promise<ICandlestick[]> {
    if (!this.exchange.fetchOHLCV) throw new Error(`No fetchOHLCV on exchange ${this.exchange.name}`)

    const allCandlesticks: ICandlestick[] = []
    const startTime = startDate.getTime()
    const endTime = endDate.getTime()
    let chunkStartTime = startTime
    let chunkEndTime = endTime

    // Ignore if timespan too small
    if (endTime - startTime <= settings.candlestickSizes[CandlestickSizes[this.candlestickSize]]) {
      return []
    }

    do {
      console.log(
        `Importing ${this.account1.symbol}-${this.account2.symbol} ${
          CandlestickSizes[this.candlestickSize]
        } candlesticks between ${new Date(chunkStartTime).toLocaleString()} and ${new Date(
          chunkEndTime
        ).toLocaleString()}...`
      )

      const candles = await this.exchange.fetchOHLCV(
        this.symbol,
        sizeToTimeframe[this.candlestickSize],
        chunkStartTime,
        1000,
        {
          end: chunkEndTime // Bitfinex
        }
      )
      const candlesticks = this.parseCandles(candles).filter(c => {
        const time = c.date.getTime()
        return time >= chunkStartTime && time <= chunkEndTime
      })

      if (candlesticks.length <= 1) {
        console.log('Ok, no more candlestick to import.')
        break
      }

      allCandlesticks.push(...candlesticks)

      const lastCandlestick = candlesticks[candlesticks.length - 1]
      console.log(
        `Imported ${candlesticks.length} ${this.account1.symbol}-${
          this.account2.symbol
        } candlesticks between ${candlesticks[0].date.toLocaleString()} and ${lastCandlestick.date.toLocaleString()}`
      )

      if (candlesticks[0].date.getTime() - chunkStartTime > chunkEndTime - lastCandlestick.date.getTime()) {
        chunkEndTime = candlesticks[0].date.getTime() - 1000
      } else {
        chunkStartTime = lastCandlestick.date.getTime() + 1000
      }
    } while (true)

    return allCandlesticks.sort((a, b) => (a.date > b.date ? 1 : -1))
  }

  private intervalCheck(delay: number) {
    this.checkTimeout = setTimeout(
      () =>
        this.checkAll()
          .catch(e => console.error(e))
          .then(() => this.intervalCheck(delay)),
      delay
    )
  }

  private async checkAll() {
    await this.checkCandles()
    await this.checkTrades()
  }

  private async checkCandles() {
    if (!this.exchange.fetchOHLCV) throw new Error(`No fetchOHLCV on exchange ${this.exchange.name}`)
    if (this.candlesticks.length === 0) return

    const since = this.candlesticks[this.candlesticks.length - 1].date

    // Fetch last candles
    const candles = await this.exchange.fetchOHLCV(
      this.symbol,
      sizeToTimeframe[this.candlestickSize],
      since.getTime() - 1000
    )

    const candlesticks = this.parseCandles(candles)
    let lastCandlestick: ICandlestick | undefined

    for (const candlestick of candlesticks) {
      const lastDate = this.candlesticks[this.candlesticks.length - 1].date
      if (candlestick.date <= lastDate) continue

      // Register candle
      this.candlesticks.push(candlestick)
      lastCandlestick = candlestick

      if (settings.verbose)
        console.log(
          'Candlestick update',
          candlestick.date.toLocaleString(),
          `: close=${candlestick.close}, volume=${candlestick.volume}`
        )
    }
    if (lastCandlestick) {
      await this.emit('candlestick', lastCandlestick)
    }
  }

  private parseCandles = (candles: number[][]) => {
    const candlesticks = candles
      .map(
        ([time, open, high, low, close, volume]): ICandlestick => ({
          date: new Date(time),
          size: this.candlestickSize,
          open,
          close,
          high,
          low,
          volume
        })
      )
      .sort((a, b) => (a.date > b.date ? 1 : -1))
    candlesticks.pop() // Remove last incomplete candlestick
    return candlesticks
  }

  private async checkTrades() {
    if (this.orders.length === 0) return

    const since = this.trades.length !== 0 ? this.trades[this.trades.length - 1].date : this.orders[0].date

    const trades: Array<
      ccxt.Trade & {
        fee: {
          cost: number
          currency: string
        }
      }
    > = await this.exchange.fetchMyTrades(this.symbol, since.getTime() - 1000)

    let traded = false

    for (const trade of trades) {
      if (this.trades.find(t => t.id === trade.id) || !trade.order) continue

      const order = this.orders.find(o => o.id === trade.order)
      if (!order) continue

      // Update balances
      if (!traded) {
        traded = true
        await this.loadBalances()
      }

      // Update order amount
      order.amount -= trade.amount

      // Calculate fees in currency2
      const fees = trade.fee.cost / (trade.fee.currency === this.account1.symbol ? trade.price : 1)

      // Register trade
      this.applyTrade(
        {
          id: trade.id,
          date: new Date(),
          currency1: order.currency1,
          currency2: order.currency2,
          side: order.side,
          rate: trade.price,
          amount: trade.amount - fees
        },
        order
      )
      await slackPostMessage(
        `Trade: ${order.side} ${trade.amount} ${order.currency2} at rate ${trade.price} ${order.currency1}`
      )
    }

    // Remove fulfilled orders
    this.orders.filter(order => order.amount <= 0.001).map(order => this.removeOrder(order))
  }
}
