type Callback = (data?: any) => Promise<void>
type Events = Map<string, Callback[]>

export default class EventPromiseEmitter {
  private events: Events = new Map()

  public on(name: string, callback: Callback) {
    const callbacks = this.events.get(name)
    if (!callbacks) this.events.set(name, [callback])
    else callbacks.push(callback)
  }

  public off(name: string, callback?: Callback) {
    const callbacks = this.events.get(name)
    if (!callbacks) return
    if (!callback) {
      this.events.delete(name)
      return
    }
    const i = callbacks.indexOf(callback)
    if (i !== -1) {
      callbacks.splice(i, 1)
    }
  }

  public async emit(name: string, data?: any) {
    const callbacks = this.events.get(name)
    if (!callbacks) return
    await Promise.all(callbacks.map(callback => callback(data)))
  }
}
