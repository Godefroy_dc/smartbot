import { EventEmitter } from 'events'
import { CandlestickSizes, IAccount, ICandlestick, IOrder, IOrderSide } from '../interfaces'
import settings from '../settings'
import Exchange from './Exchange'

export default class FakeExchange extends Exchange {
  public fees = {
    maker: 0.15,
    taker: 0.25
  }

  constructor(account1: IAccount, account2: IAccount, candlestickSize: CandlestickSizes) {
    super(
      {
        exchange: 'fakeexchange',
        currency1: account1.symbol,
        currency2: account2.symbol
      },
      candlestickSize
    )
    this.account1 = account1
    this.account2 = account2
  }

  public async addCandlestick(candlestick: ICandlestick) {
    this.candlesticks.push(candlestick)

    // Execute orders
    // Reversed-for because an order can be deleted by applyOrder
    for (let i = this.orders.length - 1; i >= 0; i--) {
      const order = this.orders[i]
      if (
        (order.side === 'buy' && order.rate >= candlestick.low) ||
        (order.side === 'sell' && order.rate <= candlestick.high)
      ) {
        const date = new Date(candlestick.date)
        date.setTime(
          date.getTime() + settings.candlestickSizes[CandlestickSizes[candlestick.size]] / 2 - 1000 * (i + 1)
        )
        this.applyOrder(order, date)
      }
    }

    // Emit candlestick
    await this.emit('candlestick', this)
  }

  public async order(side: IOrderSide, amount: number, rate?: number): Promise<IOrder> {
    // Market rate
    const market = typeof rate === 'undefined'
    if (typeof rate === 'undefined') rate = this.getMarketRate()

    // Check balance
    if (side === 'buy' && amount * rate > this.account1.availableBalance) {
      throw new Error(
        `Insufficient available balance (${this.account1.availableBalance} ${this.account1.symbol}) to buy ${amount} ${this.account2.symbol}`
      )
    } else if (side === 'sell' && amount > this.account2.availableBalance) {
      throw new Error(
        `Insufficient available balance (${this.account2.availableBalance} ${this.account2.symbol}) to sell ${amount} ${this.account2.symbol}`
      )
    }

    // Order trade
    const id = this.generateRandomId()
    const lastCandlestick = this.candlesticks[this.candlesticks.length - 1]
    const order: IOrder = {
      ...this.getTradeConfig(),
      id,
      type: market ? 'market' : 'limit',
      date: new Date(
        lastCandlestick.date.getTime() + settings.candlestickSizes[CandlestickSizes[lastCandlestick.size]]
      ),
      side,
      rate,
      amount,
      trades: [],
      events: new EventEmitter()
    }
    this.addOrder(order, true)

    if (market) this.applyOrder(order, order.date)

    return order
  }

  public async cancelOrder(order: IOrder): Promise<void> {
    order.events.emit('cancel')
    this.removeOrder(order, true)
  }

  private applyOrder(order: IOrder, date: Date) {
    this.applyTrade(
      {
        id: this.generateRandomId(),
        date,
        currency1: order.currency1,
        currency2: order.currency2,
        side: order.side,
        rate: order.rate,
        amount: order.amount
      },
      order,
      true
    )
    this.removeOrder(order, true)
  }

  private generateRandomId = () =>
    Math.random()
      .toString()
      .substr(2)
}
