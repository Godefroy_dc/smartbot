import BotAdvisor from '../botadvisors/BotAdvisor'
import Exchange from '../exchanges/Exchange'
import FakeExchange from '../exchanges/FakeExchange'
import { CandlestickSizes, ITrade } from '../interfaces'
import settings from '../settings'
import { promiseSerial } from '../utils'

export default class Bot {
  public tradingExchange: Exchange

  // Stats
  public maxDrawdown = 0
  public highestBalance = 0
  private exposureAcc = 0

  constructor(public advisor: BotAdvisor, dry = true) {
    if (dry) {
      if (settings.verbose) console.log('DRY RUN')

      // Instantiate FakeExchange to backtest
      this.tradingExchange = new FakeExchange(
        {
          symbol: advisor.tradingExchange.exchangePair.currency1,
          balance: 10000,
          availableBalance: 10000
        },
        {
          symbol: advisor.tradingExchange.exchangePair.currency2,
          balance: 0,
          availableBalance: 0
        },
        advisor.tradingExchange.candlestickSize
      )

      // Apply candlesticks from real trading exchange to fake exchange
      this.advisor.tradingExchange.on('candlestick', (exchange: Exchange) =>
        this.tradingExchange.addCandlestick(exchange.candlesticks[exchange.candlesticks.length - 1])
      )
    } else {
      this.tradingExchange = advisor.tradingExchange
    }
  }

  public start() {
    if (settings.verbose) console.log('Bot started')
    if (!this.advisor.watchedExchanges.some(exchange => exchange === this.advisor.tradingExchange)) {
      this.advisor.tradingExchange.on('candlestick', this.onCandlestick)
    }
    this.advisor.watchedExchanges.forEach(exchange => exchange.on('candlestick', this.onCandlestick))
  }

  public stop() {
    if (settings.verbose) console.log('Bot stopped')
    this.advisor.tradingExchange.stop()
    this.advisor.watchedExchanges.forEach(exchange => {
      exchange.off('candlestick', this.onCandlestick)
      exchange.stop()
    })
  }

  public async watch() {
    if (settings.verbose) console.log('Watching candlesticks...')
    await this.advisor.tradingExchange.watch()
    for (const exchange of this.advisor.watchedExchanges) {
      await exchange.watch()
    }
  }

  public getExposure() {
    return this.exposureAcc / this.tradingExchange.candlesticks.length
  }

  private onCandlestick = async (exchange: Exchange) => {
    /*
    if (settings.verbose) {
      const candlestick = exchange.candlesticks[exchange.candlesticks.length - 1]
      console.log(
        `[${candlestick.date}] ${exchange.exchangePair.exchange} ${
          exchange.exchangePair.currency1
        }/${exchange.exchangePair.currency2} candlestick - O: ${candlestick.open}, H: ${
          candlestick.high
        }, L: ${candlestick.low}, C: ${candlestick.close}, V: ${candlestick.volume}`
      )
    }
    */

    const tradingCandlesticks = this.advisor.tradingExchange.candlesticks
    if (tradingCandlesticks.length === 0) return

    const lastTradingCandlestick = tradingCandlesticks[tradingCandlesticks.length - 1]

    // Tick if last candlestick of every exchange has the same date
    if (
      this.advisor.watchedExchanges.every(
        watchedExchange =>
          watchedExchange.candlesticks.length !== 0 &&
          watchedExchange.candlesticks[watchedExchange.candlesticks.length - 1].date.getTime() ===
            lastTradingCandlestick.date.getTime()
      )
    ) {
      await this.tick()
    }
  }

  private tick = async () => {
    const rate = this.tradingExchange.getMarketRate()
    const { account1, account2, minTradeAmount } = this.tradingExchange

    // Do we want to buy?
    const advice = await this.advisor.shouldOrder()
    const orderType = advice.amountFraction > 0 ? 'buy' : 'sell'
    const adviceAmount =
      Math.abs(advice.amountFraction) *
      (orderType === 'buy' ? account1.availableBalance / rate : account2.availableBalance)

    if (adviceAmount >= minTradeAmount) {
      // Buy order at market rate
      try {
        const order = await this.tradingExchange.order(orderType, adviceAmount)

        if (advice.goalRate !== 0) {
          // Sell limit order at goal rate
          const sell = (trade: ITrade) =>
            this.tradingExchange
              .order('sell', Math.min(trade.amount, account2.availableBalance), advice.goalRate)
              .catch(e => console.error(e))

          order.events.on('trade', trade => sell(trade))
          await promiseSerial(order.trades.map(trade => () => sell(trade)))
        }
      } catch (e) {
        console.error(e)
      }
    } else {
      // Stoploss for old sell orders
      await this.stoplossCancelAndSell()
      // Stoploss buy selling all currency2 balance
      await this.stoplossSell()
    }

    // Highest balance? Max drawdown?
    const balance2 = account2.balance * rate
    const totalBalance = account1.balance + balance2
    this.exposureAcc += balance2 / totalBalance
    if (totalBalance > this.highestBalance) {
      this.highestBalance = totalBalance
    } else {
      const drawdown = (this.highestBalance - totalBalance) / this.highestBalance
      if (drawdown > this.maxDrawdown) {
        this.maxDrawdown = drawdown
      }
    }
  }

  private async stoplossSell() {
    const { trades, candlesticks, account2, minTradeAmount } = this.tradingExchange
    const { stoplossPeriod } = this.advisor.config
    if (!stoplossPeriod || trades.length === 0) return

    // If last trade is too old (stoploss), sell all
    const lastTrade = trades[trades.length - 1]
    const candlesSinceLastTrade = candlesticks.reduce((sum, c) => sum + (c.date > lastTrade.date ? 1 : 0), 0)
    if (candlesSinceLastTrade >= stoplossPeriod && account2.availableBalance > minTradeAmount) {
      await this.tradingExchange.order('sell', account2.availableBalance)
    }
  }

  private async stoplossCancelAndSell(force = false) {
    const { candlesticks, candlestickSize, orders, minTradeAmount } = this.tradingExchange
    const { stoplossPeriod } = this.advisor.config
    if (!stoplossPeriod || candlesticks.length <= 2 || orders.length === 0) return

    const lastCandlestick = candlesticks[candlesticks.length - 1]
    const candlestickDuration = settings.candlestickSizes[CandlestickSizes[candlestickSize]]
    const expirationTime = lastCandlestick.date.getTime() - stoplossPeriod * candlestickDuration

    // Get old sell orders
    const ordersToStop = orders.filter(
      order => order.side === 'sell' && (force || order.date.getTime() <= expirationTime)
    )
    if (ordersToStop.length === 0) return

    // Cancel orders
    const amountToSell = ordersToStop.reduce((sum, order) => sum + order.amount, 0)
    await promiseSerial(ordersToStop.map(order => () => this.tradingExchange.cancelOrder(order)))

    // Sell
    if (amountToSell > minTradeAmount) {
      if (settings.verbose) {
        console.log(
          `Force sell ${amountToSell} ${
            this.tradingExchange.account2.symbol
          } (old trades). Last candlestick: ${lastCandlestick.date.toLocaleString()}, Expiration date: ${new Date(
            expirationTime
          ).toLocaleString()}`
        )
      }
      await this.tradingExchange.order('sell', amountToSell).catch(e => console.error(e))
    }
  }
}
