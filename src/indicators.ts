import { ICandlestick } from './interfaces'

// Calculate RSI: Relative Strength Index
// Returns a number between 0 and 1
// See https://www.investopedia.com/terms/r/rsi.asp
export const rsi = (values: number[]) => {
  let sumGain = 0
  let sumLoss = 0
  for (let i = 1; i < values.length; i++) {
    const diff = values[i] - values[i - 1]
    if (diff >= 0) {
      sumGain += diff
    } else {
      sumLoss -= diff
    }
  }
  if (sumLoss === 0) return 1
  return 1 - 1 / (1 + sumGain / sumLoss)
}

// Calculate MFI: Money Flow Index
// Returns a number between 0 and 1
// See https://www.investopedia.com/terms/m/mfi.asp
export const mfi = (candlesticks: ICandlestick[]) => {
  let sumGain = 0
  let sumLoss = 0
  let tp = 0
  for (const candlestick of candlesticks) {
    const exTp = tp
    tp = (candlestick.high + candlestick.low + candlestick.close) / 3
    if (exTp === 0) continue
    const mf = tp * candlestick.volume
    if (tp > exTp) {
      sumGain += mf
    } else {
      sumLoss -= mf
    }
  }
  if (sumLoss === 0) return 1
  return 1 - 1 / (1 + sumGain / sumLoss)
}

// Calculate SMA: Simple Moving Average
export const sma = (values: number[]) => {
  const n = values.length
  let result = 0
  for (let i = 0; i < n; i++) {
    result += values[i]
  }
  return result / n
}

// Calculate EMA: Exponential Moving Average
export const ema = (values: number[]) => {
  const n = values.length
  const smoothing = 2 / (n + 1)
  let result = values[0]
  for (let i = 1; i < n; i++) {
    result = values[i] * smoothing + result * (1 - smoothing)
  }
  return result
}
