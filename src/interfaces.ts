import { EventEmitter } from 'events'

export interface ITrade {
  id: string
  currency1: string
  currency2: string
  side: 'buy' | 'sell'
  date: Date
  rate: number
  amount: number
}

export enum CandlestickSizes {
  min1,
  min5,
  min15,
  min30,
  h1,
  h3,
  h6,
  h12,
  d1
}

export interface IExchangePair {
  exchange: string
  currency1: string
  currency2: string
}

export interface ICandlestick {
  date: Date
  size: CandlestickSizes
  open: number
  close: number
  high: number
  low: number
  volume: number
}

export interface IAccount {
  symbol: string
  balance: number
  availableBalance: number
}

export type IOrderSide = 'buy' | 'sell'

export interface IOrder {
  id: string
  currency1: string
  currency2: string
  type: 'market' | 'limit'
  side: IOrderSide
  date: Date
  rate: number
  amount: number
  trades: ITrade[]
  events: EventEmitter
}
