import axios from 'axios'
import * as parseCsvSync from 'csv-parse'
import * as stringifyCsvSync from 'csv-stringify'
import * as fs from 'fs'
import * as PythonShell from 'python-shell'
import { promisify } from 'util'
import settings from './settings'

export const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms))

export const promiseSerial = <T>(funcs: Array<() => Promise<T>>): Promise<T[]> =>
  funcs.reduce(
    (promise, func) => promise.then(values => func().then(value => [...values, value])),
    Promise.resolve([] as T[])
  )

export const range = (start: number, end: number) => Array.from({ length: end - start }, (v, k) => k + start)
export const fill = (n: number, value: number) => Array.from({ length: n }, v => 0.5)

export interface IDated {
  date: Date
}

// Align candlesticks
export function alignDated(lists: IDated[][]): void {
  let prevTime = 0
  // Delete periods that are not in all lists
  for (let i = 0; i < minLength(lists); i++) {
    const maxTime = maxTimeAtIndex(lists, i)
    lists.forEach(list => {
      while (list[i] && (list[i].date.getTime() < maxTime || list[i].date.getTime() === prevTime)) {
        list.splice(i, 1)
      }
    })
    prevTime = maxTime
  }
  // Align lengths
  const length = minLength(lists)
  lists.forEach(list => {
    list.length = length
  })
}
const minLength = (lists: IDated[][]) => lists.reduce((min, l) => (min < l.length ? min : l.length), Infinity)
const maxTimeAtIndex = (lists: IDated[][], index: number): number =>
  lists.map(list => list[index].date.getTime()).reduce((max, time) => (max > time ? max : time), 0)

export const readFile = promisify(fs.readFile)
export const writeFile = promisify(fs.writeFile)
export const fileExists = promisify(fs.exists)
export const mkdir = promisify(fs.mkdir)

export const parseCsv = (csv: string, options?: parseCsvSync.Options): Promise<any> =>
  new Promise((resolve, reject) => {
    parseCsvSync(csv, options, (error, data) => {
      if (error) return reject(error)
      resolve(data)
    })
  })

export const stringifyCsv = (data: any[][]): Promise<string> =>
  new Promise((resolve, reject) => {
    stringifyCsvSync(data, (error, csv) => {
      if (error) return reject(error)
      resolve(csv)
    })
  })

export const runPython = (filepath: string, options?: PythonShell.RunOptions): Promise<string> =>
  new Promise((resolve, reject) => {
    const callback = (error: Error, result: any) => {
      if (error) return reject(error)
      resolve(result)
    }
    if (options) PythonShell.run(filepath, options, callback)
    else PythonShell.run(filepath, callback)
  })

export const slackPostMessage = (message: string) =>
  settings.slack.recipients.forEach(recipient =>
    axios
      .post(
        settings.slack.slackbotUrl +
          '?token=' +
          settings.slack.slackbotToken +
          '&channel=' +
          encodeURIComponent(recipient),
        message
      )
      .catch(e => console.error(e))
  )
