// Standardize features by removing the mean and scaling to unit variance

export interface IScaled {
  // Scaled dataset
  dataset: number[][]
  // Original mean and standard deviation per feature
  stats: IScaleStats[]
}

export interface IScaleStats {
  mean: number
  std: number
}

/* Example of dataset:
   dataset of 3 entries, 2 features
   [[1,2], [3,4], [5,6]]
   Scaling is done by feature
*/

export function scaleStd(dataset: number[][], noMeanChange = false): IScaled {
  if (dataset.length === 0) return { dataset, stats: [] }

  // Reserve dataset to separate by feature
  const reversedDataset = reverseDataset(dataset)
  const stats: IScaleStats[] = []

  // Iterate on features
  for (const values of reversedDataset) {
    const valuesMean = noMeanChange ? 0 : mean(values)
    const valuesStd = std(values, valuesMean) || 1

    // Scale values
    for (let i = 0; i < values.length; i++) {
      values[i] = (values[i] - valuesMean) / valuesStd
    }

    stats.push({
      mean: valuesMean,
      std: valuesStd
    })
  }

  return {
    dataset: reverseDataset(reversedDataset),
    stats
  }
}

export function scale(dataset: number[][], stats: IScaleStats[], reverse = false): number[][] {
  if (dataset.length === 0) return dataset

  // Reserve dataset to separate by feature
  const reversedDataset = reverseDataset(dataset)

  // Iterate on features
  for (let j = 0; j < reversedDataset.length; j++) {
    const stat = stats[j]
    const values = reversedDataset[j]
    for (let i = 0; i < values.length; i++) {
      if (reverse) {
        values[i] = values[i] * stat.std + stat.mean
      } else {
        values[i] = (values[i] - stat.mean) / stat.std
      }
    }
  }

  return reverseDataset(reversedDataset)
}

function reverseDataset(dataset: number[][]) {
  return dataset[0].map((_, i) => dataset.map(d => d[i]))
}

// Sum of numbers
export function sum(values: number[]): number {
  return values.reduce((s, n) => s + n, 0)
}

// Mean (average)
export function mean(values: number[]): number {
  if (values.length === 0) return 0
  return sum(values) / values.length
}

// Standard deviation
export function std(values: number[], valuesMean?: number): number {
  if (values.length === 0) return 0
  const vMean = typeof valuesMean === 'number' ? valuesMean : mean(values)
  return Math.sqrt(
    values.reduce((s, value) => {
      const k = value - vMean
      return s + k * k
    }, 0) / values.length
  )
}
