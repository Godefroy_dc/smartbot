import {
  EntityRepository,
  Repository,
  TransactionManager,
  Transaction,
  EntityManager
} from 'typeorm'
import { Candlestick } from '../entity/Candlestick'
import { CandlestickSizes, IExchangePair, ICandlestick } from '../../interfaces'

@EntityRepository(Candlestick)
export class CandlestickRepository extends Repository<Candlestick> {
  public async getFirstCandlestick(
    exchangePair: IExchangePair,
    size: CandlestickSizes
  ): Promise<Candlestick | null> {
    const candlesticks = await this.find({
      where: {
        exchange: exchangePair.exchange,
        currency1: exchangePair.currency1,
        currency2: exchangePair.currency2,
        size
      },
      order: { date: 'ASC' },
      take: 1
    })
    return candlesticks.length !== 0 ? candlesticks[0] : null
  }

  public async getLastCandlestick(
    exchangePair: IExchangePair,
    size: CandlestickSizes
  ): Promise<Candlestick | null> {
    const candlesticks = await this.find({
      where: {
        exchange: exchangePair.exchange,
        currency1: exchangePair.currency1,
        currency2: exchangePair.currency2,
        size
      },
      order: { date: 'DESC' },
      take: 1
    })
    return candlesticks.length !== 0 ? candlesticks[0] : null
  }

  public async getCandlesticks(
    exchangePair: IExchangePair,
    size: CandlestickSizes,
    startDate: Date,
    endDate: Date
  ): Promise<Candlestick[]> {
    return this.createQueryBuilder()
      .where({
        exchange: exchangePair.exchange,
        currency1: exchangePair.currency1,
        currency2: exchangePair.currency2,
        size
      })
      .andWhere('date >= :startDate AND date <= :endDate', { startDate, endDate })
      .orderBy({ date: 'ASC' })
      .getMany()
  }

  /*
  public async saveCandles(exchangePair: IExchangePair, candles: ICandlestick[]) {
    await this.save(candles.map(c => Candlestick.fromICandlestick(exchangePair, c)))
  }
  */

  @Transaction()
  public async saveCandles(
    exchangePair: IExchangePair,
    candles: ICandlestick[],
    @TransactionManager() manager?: EntityManager
  ) {
    if (!manager) throw new Error('No transaction manager')

    // Separate requests to avoid limit of parameters number in prepared statement
    for (let i = 0; i < candles.length; i += 1000) {
      await manager.save(
        candles.slice(i, i + 1000).map(c => Candlestick.fromICandlestick(exchangePair, c))
      )
    }
  }

  /*
  public async saveCandles(exchangePair: IExchangePair, candles: ICandlestick[]) {
    await this.createQueryBuilder()
      .insert()
      .into(Candlestick)
      .values(
        candles.map(candle => ({
          ...candle,
          exchange: exchangePair.exchange,
          currency1: exchangePair.currency1,
          currency2: exchangePair.currency2
        }))
      )
      .execute()
  }
  */
}
