import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm'
import { CandlestickSizes, ICandlestick, IExchangePair } from '../../interfaces'

@Entity()
export class Candlestick {
  public static toICandlestick = (candlestick: Candlestick): ICandlestick => ({
    date: candlestick.date,
    size: candlestick.size,
    open: candlestick.open,
    high: candlestick.high,
    low: candlestick.low,
    close: candlestick.close,
    volume: candlestick.volume
  })

  public static fromICandlestick = (exchangePair: IExchangePair, candle: ICandlestick): Candlestick => {
    const candlestick = new Candlestick()
    candlestick.exchange = exchangePair.exchange
    candlestick.currency1 = exchangePair.currency1
    candlestick.currency2 = exchangePair.currency2
    candlestick.date = candle.date
    candlestick.size = candle.size
    candlestick.open = candle.open
    candlestick.high = candle.high
    candlestick.low = candle.low
    candlestick.close = candle.close
    candlestick.volume = candle.volume
    return candlestick
  }

  @PrimaryGeneratedColumn()
  public id: number

  @Column()
  @Index()
  public exchange: string

  @Column()
  @Index()
  public currency1: string

  @Column()
  @Index()
  public currency2: string

  @Column('timestamp with time zone')
  @Index()
  public date: Date

  @Column('smallint')
  public size: CandlestickSizes

  @Column('double precision')
  public open: number

  @Column('double precision')
  public close: number

  @Column('double precision')
  public high: number

  @Column('double precision')
  public low: number

  @Column('double precision')
  public volume: number
}
