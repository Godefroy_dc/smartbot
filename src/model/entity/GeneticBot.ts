import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'
import { Population } from '../../botadvisors/genetic/engine/evolve'
import { IGeneticBotConfig, IGeneticEntity, IGeneticFitness } from '../../botadvisors/genetic/interfaces'

@Entity()
export class GeneticBot {
  @PrimaryGeneratedColumn()
  public id: number

  @Column('jsonb')
  public config: IGeneticBotConfig

  @Column('jsonb')
  public population: Population<IGeneticEntity, IGeneticFitness>
}
