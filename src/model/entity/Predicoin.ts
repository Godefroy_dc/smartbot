import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Predicoin {
  @PrimaryGeneratedColumn()
  public id: number

  @Column()
  public timestamp_pst: Date

  @Column()
  @Index()
  public timestamp_utc: Date

  @Column()
  @Index()
  public ticker: string

  @Column('double precision', { nullable: true })
  public price_usd: number

  @Column('double precision', { nullable: true })
  public price_btc: number

  @Column('double precision', { nullable: true })
  public market_cap_usd: number

  @Column('double precision', { nullable: true })
  public volume_usd: number

  @Column('double precision', { nullable: true })
  public overall_score: number

  @Column('double precision', { nullable: true })
  public fundamental: number

  @Column('double precision', { nullable: true })
  public technical: number

  @Column('double precision', { nullable: true })
  public technical_raw: number

  @Column('double precision', { nullable: true })
  public news_sentiment: number

  @Column('double precision', { nullable: true })
  public news_sentiment_raw: number

  @Column('double precision', { nullable: true })
  public news_volume: number

  @Column('double precision', { nullable: true })
  public twitter_sentiment: number

  @Column('double precision', { nullable: true })
  public twitter_sentiment_raw: number

  @Column('double precision', { nullable: true })
  public twitter_volume: number

  @Column('double precision', { nullable: true })
  public reddit_sentiment: number

  @Column('double precision', { nullable: true })
  public reddit_sentiment_raw: number

  @Column('double precision', { nullable: true })
  public reddit_volume: number

  @Column('double precision', { nullable: true })
  public buzz: number

  @Column('double precision', { nullable: true })
  public buzz_raw: number

  @Column('double precision', { nullable: true })
  public popularity: number

  @Column('double precision', { nullable: true })
  public popularity_price_ratio: number

  @Column('double precision', { nullable: true })
  public search_engines_trend: number

  @Column('double precision', { nullable: true })
  public search_engines_popularity: number

  @Column('double precision', { nullable: true })
  public news_trend: number

  @Column('double precision', { nullable: true })
  public news_popularity: number

  @Column('double precision', { nullable: true })
  public twitter_trend: number

  @Column('double precision', { nullable: true })
  public twitter_popularity: number

  @Column('double precision', { nullable: true })
  public reddit_trend: number

  @Column('double precision', { nullable: true })
  public reddit_popularity: number

  @Column('double precision', { nullable: true })
  public developer_activity: number

  @Column('double precision', { nullable: true })
  public developer_community: number

  @Column('double precision', { nullable: true })
  public ta_moving_averages: number

  @Column('double precision', { nullable: true })
  public ta_oscillators: number
}
