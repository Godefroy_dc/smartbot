# Trading Bot

# Install

## Dependencies

Install these programs:

- Node 8+: https://nodejs.org/
- PostgreSQL 10+: https://www.postgresql.org/
- Python 3.6 amd64: https://www.python.org/

and Tensorflow dependencies (check exact versions: https://www.tensorflow.org/install/install_windows):

- Nvidia Cuda 9.0: https://developer.nvidia.com/cuda-toolkit-archive
- Nvidia CuDNN v7 for Cuda 9.0: https://developer.nvidia.com/rdp/cudnn-download & https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html

Install python dependencies:

```sh
pip install pandas numpy matplotlib keras sklearn tensorflow-gpu h5py
```

Install node dependencies:

```sh
yarn install
```

## Database

Create user and database:

```sql
CREATE DATABASE smartbot;
CREATE USER smartbot WITH PASSWORD 'smartbot';
GRANT ALL PRIVILEGES ON DATABASE smartbot TO smartbot;
```

The schema of database is created/updated automatically by TypeORM.

## Configuration

Check `src/settings.ts` for configuration.

Create `.env` file with API keys of exchanges configured in `src/settings.ts`.

# Usage

Train Genetic Bot:

```sh
yarn genetic-train
```

Backtest Genetic Bot:

```sh
yarn genetic-backtest
```

Run (live) Genetic Bot:

```sh
yarn genetic-live
```

# Bot

## Goal

Learn to detect risk free increases in price.

## Behaviour

Buy $tradeAmount when confidence > $minConfidenceToBuy.
Sell when we meet the goal, and confidence < \$maxConfidenceToSell

## Inputs

1 neuron / indicator / pair / period:

- Pairs
  - ETHEUR
  - BTCEUR
- Indicators
  - high
  - low
  - number of trades
  - volume
- Period:
  - 5 min
  - 15 min
  - 30 min
  - 1h
  - 2h
  - 3h
  - 6h
  - 12h
  - 1 day
  - 2 days
  - 3 days

## Outputs

- confidence [0, 1] of a $goalMove % increase during the next $goalPeriod

## Paramètres

- \$amount = balance _ 0.1 _ confidence
- \$minConfidenceToBuy = 0.7
- \$maxConfidenceToSell = 0.7
- \$goalMove = 10%
- \$goalPeriod = 60 \* 24 minutes

# SQL Queries

## Find duplicates

```sql
select date, count(date) as nb from candlestick where currency2 = 'EOS' group by date having (count(date) > 1);
```

## Find missing candlesticks

```sql
select c.currency2, c.date from candlestick c left join candlestick c2 on c2.date = c.date and c2.currency1 = c.currency1 and c2.currency2 != c.currency2 where c2 is null;
```

# Successful testing params

## Test 1

**Training**

- Pair: USD-BTC
- Period: 2017-01-01 - 2017-11-01
- Candlesticks: 30min
- Goal Period: 3
- Goal Move: 4.4

**Testing**

- Initial Balance: 1000 USD, 0 BTC
- Buy threshold: 0.89
- Stoploss after goal period

## Test 2

Crash resistant: tested on 2017-12-18 on 2018-02-06
Consistant return of 12% / month

**Training**

- Pair: USD-BTC
- Period: 2017-06-01 - 2018-01-01
- Candlesticks: 30min
- Goal Period: 3
- Goal Move: 3

**Testing**

- Initial Balance: 1000 USD, 0 BTC
- Buy threshold: 0.89
- Amount traded depends on guess
- Goal rate depends on guess
- No Stoploss

## Todo

- Remove useless Learning Rate
- Remove Candlesticks column
- Remove learning rate
